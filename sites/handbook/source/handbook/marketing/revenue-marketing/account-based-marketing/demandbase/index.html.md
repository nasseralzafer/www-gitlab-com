---
layout: handbook-page-toc
title: "Demandbase"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Demandbase
Demandbase is a complete end-to-end solution for [Account-Based Marketing](/handbook/marketing/revenue-marketing/account-based-marketing/). We primarily use Demandbase as a targeting and personalization platform to target online ads to companies that fit our ICP and tiered account criteria. Demandbase also has a wealth of intent data that is available to us through its integration with Salesforce. 

We compile the intent data by building audiences, groups of target accounts with the most potential to purchase based on our buyer criteria, which we can then leverage for use throughout the funnel in advertising and SDR/sales follow-up. Demandbase also delivers ongoing signals around behaviors and intent to keep our list up to date. This information helps both marketing and sales focus our efforts on the right accounts. 

## Use cases and features
To date, we use the following Demandbase features. In addition to the general training resources section below, each use case will also have a section on this page with more information and training resources. 
- [ABM platform](https://support.demandbase.com/hc/en-us/sections/360003540751-ABM-Platform-Solution): identify the companies that meet your buying criteria and are showing the right intent signals
- [Targeting](https://support.demandbase.com/hc/en-us/sections/360009465252-Getting-Started-with-Self-Serve-Targeting): Reach the full buying committee and nurture your best-fit accounts from awareness to revenue with account-based advertising. Easily set up, launch, and manage campaigns.
- [Conversion](https://support.demandbase.com/hc/en-us/sections/360003566352-Conversion-Solution): give SDR and Sales teams the data and insights they need to better understand and target the right individuals, with personalized messages, within their target accounts, so they can close deals faster.
- Analytics: 
     - [ABM analytics](https://support.demandbase.com/hc/en-us/sections/360003540851-ABM-Analytics-Solution): focus on business outcomes and report on account-level reach, engagement, and sales outcomes.
     - [Site analytics](https://support.demandbase.com/hc/en-us/sections/360009288811-Site-Analytics-Solution): provide insight around channel performance (i.e. - display advertising, traffic, campaign influence) 

## General Demandbase training resources
- Demandbase Certification: Go to the [Demandbase training portal](https://training.demandbase.com/) > navigate to ‘Enroll in Certifications’ > ‘Demandbase Solutions Certification’
- [Training resources library](https://support.demandbase.com/hc/en-us/categories/360001449332-Learn-to-Use): videos and articles for getting started with Demandbase. 
- [Demandbase glossary](https://support.demandbase.com/hc/en-us/articles/360022927111-Demandbase-Glossary): list of key terms and their definitions.
- [Introduction to ABM](https://support.demandbase.com/hc/en-us/articles/360037346672-ABM-Introduction): what is ABM? 

## Demandbase ABM Platform
The [Demandbase ABM Platform](https://support.demandbase.com/hc/en-us/sections/360003540751-ABM-Platform-Solution) lets you discover and manage audiences of target accounts, measure the progress of those accounts and act on them across the entire funnel.

### Audience
An audience is list of accounts. We build an audience to see a list of target accounts we want to look at intent for or run a campaign against. Target accounts are those companies with the most potential to purchase. There are two types of audiences in the ABM Platform: Dynamic and Static.
- A Dynamic Audience is a list of target accounts that changes based on behaviors or rules we specify. What makes the audience dynamic is that the accounts included in the audience change as criteria are met. For example, if you specify that you only want accounts that have visited your web page in the last 30 days, then as time passes, only the accounts that fit that criteria will be included in the audience list. 
- A Static Audience is a list of target accounts that have met your initial criteria but do not change over time. 

### Audience best practices
- It is not best practice to use the CSV upload method as you will have to manually manage this audience and it is difficult to get this information to anyone outside of the Demandbase platform. Please use the other methods.
- Follow the naming convention listed below when giving your audience a name.
- When creating an audience, at step 2, you will always choose the GitLab [profile](https://support.demandbase.com/hc/en-us/articles/360022927111-Demandbase-Glossary#StepP). This has been created in partnership with marketing teams across GitLab and is the most up to date profile. 
- When creating an audience, step 4, no matter your method, is always optional. We don’t use this step often, but with step 4 you are able to:
     - Only see accounts with a certain number of page views or days since an account was last on our site. 
     - Select specific intent keywords from the GitLab profile instead of running your audience against all of GitLab’s intent keywords. 

### How to build an Audience
There are several ways to build an audience in Demandbase depending on what you are looking to accomplish.
- Step by step instructions for each method can be found on [this](https://support.demandbase.com/hc/en-us/articles/115005093986-Create-a-New-Audience) documentation page. 
1. CRM Report- you have a target account list that may be updated as the campaign progress.  These audiences are refreshed every Friday and will reflect any changes to the CRM report as such.
1. AI - Demandbase artificial intelligence (AI) technology recommends target accounts for you based on your firmographic data filters.
1. Site Analytics- create an audience based on intent using page views, hits to a certain page, etc.  This is a good way to build an audience if you are looking for a set of accounts that behave similarly or are researching the same content. Steps to create an audience in Site Analytics can be found [here](https://support.demandbase.com/hc/en-us/articles/360039832352-Working-with-Site-Analytics#h_8ef9f39f-0bca-4f8e-babe-01225c1abee1). 
1. CSV upload- create an audience by uploading a CSV file to Demandbase.  This should be used with caution as any additions to the audience will need to be manually made both in Demandbase and Salesforce in order to report correctly on the campaign.

#### Audience naming convention:
The name of an audience should tell you who the DRI is (team) and what the list is for.  Audience names are editable so if you are unsure, please name using the following criteria and reach out to the ABM team on slack.

**Example: (FM) 20200901_SecurityWorkshop**

1. Team DRI for the list should be identified by the following:
- (ABM) Account Based Marketing & Strategy
- (FM) Field Marketing
- (DMP) Digital Marketing Programs
- (M) General Marketing (includes growth, alliance, demand gen, etc)
- (S) Sales requests
2. Name of the list- what does this list contain?
- campaign tag if it is an audience specific to a campaign
- intent based etc
- Segment- if an audience is a segment of a larger audience.  example: if you wanted to create a segment of this audience: (FM) 20200901_SecurityWorkshop it would be named (FM) Segment-20200901_SecurityWorkshop

### Sharing an Audience
- To date, there are three methods for viewing an audience and its data:  
1. Viewing the audience directly within the Demandbase web app. If you have access to Demandbase, log in and navigate to the audience tab. You can search audiences at the left-hand side and click in to any audience for further detail. 
1. Export the audience: in the Demandbase web app, click into the audience > click export audience in the top right-hand corner. You will receive an email with a link to your audience CSV. 
1. View the audience in Salesforce within the Demandbase visual force tab. On your SFDC home page click the '+' sign to the far right of your navigation tabs and then select Demandbase. This view is not customizable. 

### Audience type use cases
| Source | Dynamic or Static? | Best for |
| ------ | ------ | ------ |
| CRM Report | Dynamic | for account list that may have changes, or if accounts are being added as the campaign progresses |
| CSV file upload | Static | there is a set target list for a campaign that will not change |
| Site Analytics | Dynamic | based on intent using page views, hits to a certain page, etc. |
| Segment of a larger audience | Dynamic | If you want to create a campaign for a subset of an audience |

## Demandbase Targeting
[Demandbase Targeting Solution](https://support.demandbase.com/hc/en-us/sections/360009465252-Getting-Started-with-Self-Serve-Targeting) helps us reach the actual buyers in our target accounts with relevant ads on brand-safe sites to execute account-based marketing campaigns.
### Use Cases
Account-Based Advertising: Uses your target account list to help you display personalized advertising across many different channels.
Ad Personalization: Helps you to dynamically deliver personalized ads to selected accounts.
Keyword Discovery: Helps you discover new areas of intent for buyers at your target accounts.
Campaign Creation and Reporting: Helps you to set up ad campaigns and measure the results of your ad campaign against business outcomes. 
### Demandbase Display Campaigns
Demandbase has a programmatic display platform that enables personalized display advertising for target accounts using its own internal DSP. Demandbase Display ads are different from [the standard Paid Display ads](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#display-ads) we typically run in Google Display Network for demand generation. Display campaigns excel in reach, brand awareness, and engagement, so coupled with Demandbase's account-centric targeting, DB campaign objectives expand to include targeted account penetration.
 We partner directly with Demandbase and do not run these campaigns through PMG, our paid advertising digital agency.

### Demandbase Campaign Process
If you would like to target named accounts with paid display ads, create an issue with the appropriate Demandbase Campaign Request template:
- [Campaign Request issue template for Field and Corporate Marketing](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=Demandbase_Campaign_Request)
- [General Campaign Request issue template (for all other teams)](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/new?issuable_template=Demandbase_Campaign_Request_Template)

The DMP will then conduct a reach test with Demandbase to determine if your selected accounts are reachable, so please provide estimated budget, campaign duration, geo target, and a list of accounts [formatted for DB (Demandbase)](https://docs.google.com/spreadsheets/d/1sJvmzdhL8k5N6WQPSlE3ndAF0jL8MUTuZcD6dM-1D0w/edit?usp=sharing). If your reach test comes back too low, the DMP will recommend that you add more accounts, expand your geo target, increase your campaign duration, and/or double check to make sure your list is formatted correctly for Demandbase upload. After the reach test confirms reachable accounts and available display inventory, the DMP will set up the campaign in Demandbase by selecting the audience, layering on geo targeting, and uploading the display images & landing URL(s). Once the campaign is submitted, it will go live within 1-2 business days.

If your campaign objective is promoting an event or webcast, we recommend launching one month in advance of the event date in order to increase awareness & reach. If your campaign objective is account penetration, we recommend running your campaign as “always-on” to build awareness for your priority accounts and optimize over time with relevant assets. The DMP will provide a UTM report to track inquiries/registrations attributed to your DB campaign. Front-end Demandbase metric reports are also available upon request.


### Demandbase Ads
#### Display Ad Specs
- 728x90
- 300x250
- 160x600
- 300x600
- 970x250

#### Display Ad Copy
- **Top of funnel:** Use a broad value proposition to increase awareness and education, assert your brand value and why prospects should care.
- **Middle of funnel:** Include a tailored value proposition, provide practical how-to content, best practices, and tips and tricks.
- **Bottom of funnel:** Reinforce the value proposition, go into further detail about your offerings with case studies, ROI calculators, and product and solution content.
- **All stages:** Keep it simple and have a clear CTA.

#### Demandbase Campaign Best Practices
##### Campaign Run Time:
- Event promotion: 1 month minimum
- Target account penetration: 2-3 months minimum
- Longer advertising campaigns that span the full funnel can help build awareness and create more engagement. The result is a higher number of accounts that go into the pipeline with a higher average booking value.

##### Budget:
- [Demandbase funnel budgeting](https://docs.google.com/presentation/d/1swa9gWFAttRAas5feGSWPgsbx1HyUwiNtTXYiOL47wE/edit#slide=id.g810b16a717_0_0)
 - Start with a low monthly budget for top of funnel campaigns and increase the budget as you progress to mid and lower funnel campaigns.

##### Audiences:
- You can segment by stage in the buyer’s journey, company size, industry, etc. The core message for each of these groups is different, therefore they shouldn’t receive the same ad creative.
- Segment audiences into High, Medium, and Low intent based on intent data results

### Digital Campaign Metrics & Performance
- **Impressions:** Total ad views served
- **Clicks:** Total ad clicks
- **CTR:** Click-through rate. -> Formula: Clicks divided by Impressions
- **CPM:** Average cost per one thousand impressions
- **CPC:** Cost per click. -> Formula: Cost divided by Clicks
- Top Publishers
- Top Performing Creative
- Top Engaged Accounts

Since Demandbase is an ABM-oriented DSP, it self-optimizes for on-site engagement rather than traditional display metrics like CTR. We can check engagement metrics after two weeks, once the campaign has stabilized from initial launch, similar to learning phases in paid social. 

Budget pacing is spread out based on campaign duration, but can rise or dip based on inventory. It will ultimately hit the budget with no overages in the end.

### Demandbase Campaigns for Field Marketing Promotions
To use Demandbase solely for event promotion can increase brand lift for accounts, but it won't match the volume of registrations paid social can generate. Due to registration inefficiencies by using Demandbase only for promotions, we instead recommend using Demandbase to support & enhance account lead gen tactics (paid social, email, etc) with brand awareness, and gather account activity data to measure account penetration. We recommend either running Demandbase in conjunction with lead gen tactics, or running standalone ongoing Demandbase campaigns to warm up audiences before targeting the same audience with lead gen ads for events. Both recommendations allow us to continuously gather data in order to form smarter campaigns down the line.

We suggest using ABM Analytics & Account Stage reporting to show account engagement & influence. Although brand lift can be difficult to measure, you can see how accounts advanced through the funnel during Demandbase Display campaigns. For Field Marketers, ongoing training can help this team utilize Demandbase in order to parse account data & help with regional strategy.

## Analytics 
### Demandbase ABM Analytics 
[ABM Analytics](https://support.demandbase.com/hc/en-us/articles/360005054311-ABM-Analytics-Overview) is a native analytics tool within the Demandbase platform. It gives you insight into how your target accounts are performing across your full marketing funnel from engagement to conversion to closed won. 

#### How to use ABM Analytics
- [Working with the ABM Analytics Dashboard](https://support.demandbase.com/hc/en-us/articles/360005054311-ABM-Analytics-Overview#h_5966557921550945944419)
- [Steps for Using ABM Analytics](https://support.demandbase.com/hc/en-us/articles/360005054311-ABM-Analytics-Overview#h_578082746371550945977821)

#### Opportunity Reports Manager
The [Opportunity Reports Manager](https://support.demandbase.com/hc/en-us/articles/360036023532-Working-with-Opportunity-Reports-Manager) allows you to customize which opportunities are used in your reports and analytics within ABM Analytics. To date, you can only filter/segment by opportunity stage/type/status. 

Our Demandbase instance already has a report for each opportunity stage. This lets you select an audience (list of accounts) and ‘filter’ it by opportunity stage. 

##### How to filter using the Opportunity Reports feature
1. Create an audience or leverage one of the audiences already in our DB instance that has all of the accounts you want to see.
2. In DB, navigate to the 'ABM Analytics' tab.
3. Select the audience you want to see the data for from the 'primary audience' dropdown.
4. To narrow the audience by opp stage, check the box 'Filter results by Opportunity Report' and select the opp stage from the dropdown.

#### ABM Metrics
- **Lifted Accounts**: Percentage of the target accounts that have more engagement (page views) during the campaign(s) compared to the baseline period of 30-days prior to the start of the campaign(s). Baseline page view counts are normalized for campaign length in calculating this metric.
- **Page Views:** Total page views on your website during the campaign(s)
- **% increase in Page Views:** Percent change in page views during the campaign(s), compared to baseline period of the 30 days preceding the start of the campaign. *Note: Baseline page view counts are normalized for campaign length in calculating this metric.
- **Account Performance by Stage:**
     - **Total Accounts:** The total number of accounts being targeted
     - **Reach:** he total number of accounts that have been served at least one impression
     - **Visited:** The total number of accounts that have been on site during the campaign(s)
     - **Clicked:** The total number of accounts from which clicks have been generated during the campaign(s)
     - **Engaged:** The total number of targeted accounts that have had three or more unique sessions within a 30-day period.
     - **Converted:** TBD
     - **Opportunity:** The total number of accounts with at least one new CRM opportunity created during the campaign(s)
     - **Won:** The total number of accounts with at least one CRM opportunity that has progressed to Closed/Won during the campaign(s)

### Demandbase Site Analytics
Demandbase Site Analytics gives you website analytics with an account-based lens so you can better evaluate website performance and personalize marketing efforts to them. 

#### Use Cases
- Create and target specific audiences with relevant content
- Gain insights on which site content and web pages are most valued by visitors from your target accounts
- Use UTM and URL parameters to track channel and campaign performance over time
- Build campaigns around specific levels of website activity
- Drive registration for upcoming events: discover who has visited your event pages, but has not registered for an event

#### How to use Site Analytics
- Steps for using Site Analtyics as well as building an audience from Site Analytics can be found [here](https://support.demandbase.com/hc/en-us/articles/360039832352-Working-with-Site-Analytics#h_8ef9f39f-0bca-4f8e-babe-01225c1abee1). 

## Demandbase & Salesforce 
#### Salesforce Metrics

- **[SAO](/handbook/business-ops/resources/#glossary): Sales Accepted Opportunity:** Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting
- **[Closed Won](/handbook/business-ops/resources/#opportunity-stages):** The terms have been agreed to by both parties and the quote has been approved by Finance.

### Salesforce intent fields & definitions

In Salesforce, you will see a set of fields at the account level under the Demandbase section.  Below is a list of those fields that are populated by Demandbase and their definitions.

**Score** Demandbase uses AI to qualify accounts and give them a score based on their match to our ICP and their engagement on our website, and also offsite intent.  Scores are ranked High, Medium, and Low.  Demandbase's AI uses our ideal customer profile (Intent Keywords/Buyer Titles/Customers) to model and score/rank accounts based on:

*  Intent from target accounts

*  Buying committee titles within profile

*  Firmographics of customers in profile (Revenue, industry, geo location, employee size, etc.)

*  Products sold by customers in profile

**Trending onsite engagement** Shows us which accounts have been more engaged and active on our site over the last week compared to the last two months.

**Page views** The number of unique page views within the last thirty days.  We have scrubbed pages such as the careers page etc that do not show an intent to research or purchase GitLab.

**Sessions** Shows the number of unique visits the account has made to our website in the last thirty days.  A session is defined as a unique visit to our site of up to 30 minutes.  If they spend more than 30 minutes on our site this will count as two sessions.  It is generally considered that an account is engaged if they have three or more sessions in the past thirty days.

**Last Seen** The number of days since an account was last on our website.

**Rank** How an account ranks based on our profile within Demandbase

**Intent** This field will show you the top five keywords the account is researching

**Trending offsite intent** Shows if the account has had a recent spike in offsite research (in the last week compared to the last two months), including competitor research

**Change traffic MOM** Percentage of change, either increase or decrease of traffic on our website from the account month over month.

## Demandbase Conversion 
Demandbase’s technology uses AI and machine learning to process large amounts of data through a set of specific inputs. The result of this process is timely, relevant, and unique insights. Using these insights, SDR and sales teams can prioritize their outreach and send personalized messages to the targeted individuals. These insights are delivered to the the tools Sales teams use every day including: email, Salesforce Conversion App, and Slack.

### How to use Conversion
Setup and update your notification preferences by following [these](https://support.demandbase.com/hc/en-us/articles/360020382852-Adjust-Your-Conversion-Insights) instructions.

### Use cases
1. Prioritize target accounts 
2. Understand interestes before the prospect raises their hand or registers as a lead
3. Create more personalized experiences for prospects

### Delivery methods
- Weekly Email Digest 
- Salesforce.com insights (found as a section on the account object for matched accouts)
- Slack alerts (real time)

### Best practices
**Email Digest: Best Practices**
- Sent weekly towards the beginning of the week
- Treats on-site activity or intent like a form fill (without linking to an individual prospect)
- Used to prioritize accounts that are showing an increase in engagement
- Direcly links to LinkedIn Sales Navigator, SFDC, and locations
- Use sales navigator, location data, and salesforce history to identify the most relevant prospects
- Use intent insights to create personalized outreach

**Slack Alerts: Best Practices**
- Shows most recent web activity and real-time account interest
- Helps to better understand effectiveness of email messaging
- Easily access LinkedIn Sales Navigator, Salesforce, and preferences
- Use for real-time account prioritization 
- Use for improved personalization and engaging accounts that are already showing positive buying signals

**Salesforce.com Integration**
- Sum of activity from weekly email digest
- Review web traffic over the last 30 days
- Cross-reference alerts with SFDC account & opportunity history
- Helps to prioritize and personalize
- Review locations where the engagement is happening
- Reach out to best-fit prospects from locations showing higher engagement trends
