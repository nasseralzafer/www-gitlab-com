---
layout: handbook-page-toc
title: "Enterprise Applications Team"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fas fa-newspaper" id="biz-tech-icons"></i> Who We Are

The **Enterprise Applications Team** implements and supports specialized applications that support our business processes within GitLab.  

We are directly responsible for all of GitLab's finance systems and Enterprise Applications Integrations.  We build and extend these applications to support the processes of our business partners and rationalize our application landscape to ensure it is optimized for efficiency and spend. 

Our team ensures the availability of these applications and integrations through monitoring and alerting. These internal-facing applications include a multitude of different applications and environments, including Zuora, Adaptive Planning, Netsuite, Expensify, and etc. (tech stack).  Also we are responsible for the IT Audit and Compliance function to ensure we pass SOX Audit for our IT General Controls (ITGC).  

# <i class="fas fa-tasks" id="biz-tech-icons"></i> Vision

- To enable end to end business processes within the enterprise applications that seamlessly hand off to each other and ensure it provides a great user experience to our business partners
- Ensure data integrity between systems  and security of that data
- Constantly iterate to simplify and ensure processes are efficient and automated as much as possible..
- Leveraging out of the box best practices as much as possible. We buying and extend applications where we don't see building them as GitLabs core engineering competency
- IT Audit and Compliance - Ensuring that all customer / business data is secure and can pass key audits for attestations and compliance with SOX, SOC, etc.


# <i class="fas fa-bullhorn" id="biz-tech-icons"></i> Services we offer

##### Business Analysis
* Being business process first, means that the Enterprise Applications team will firm up requirements, use cases, and process flows as we implement systems, enhance them or deliver fixes.   Learn more here (Link Coming Soon). 

##### Implementation Management
*  Our team will align with vendor teams to implement Enterprise Applications that are coming on board. We follow a process that ensures we keep multiple groups aligned as we iterate to get the systems up quickly and efficiently. Learn more here (Link Coming Soon). 

##### Finance Systems Admin
*  Enterprise Applications supports all of the core finance systems with experienced admins that streamline and enhance current processes, turn on new features, and improve end to end process cycle time. 

##### Integration Engineering
* Our integration team manages all of the integrations between Enterprise Applications at GitLab.  Focusing on building out failover, redundant and auditable integrations that are constantly monitored. Learn more [here] (https://about.gitlab.com/handbook/business-ops/enterprise-applications/architecture/integrations/).

##### IT Audit and Compliance
* Focusing on operationalizing and optimizing the Information Technology General Compute Controls (ITGCs) for Gitlab.  This is a critical step to supporting our security posture and meeting SOX compliance and becoming a public company. Learn more [here] (https://about.gitlab.com/handbook/business-ops/it-compliance/.

# <i class="fas fa-users" id="biz-tech-icons"></i> The team

<div class="flex-row" markdown="0" style="height:80px">
  <a href="/job-families/finance/business-system-analyst/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Systems Analyst</a>
  <a href="/handbook/business-ops/it-compliance/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">IT Compliance</a>
  <a href="/job-families/finance/finance-systems-administrator" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Finance Operations</a>
  <a href="/handbook/business-ops/enterprise-applications/architecture/integrations/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Integrations Engineering</a>
</div>

## <i class="far fa-building" id="biz-tech-icons"></i> Results we are focused on!


#### Finance

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1580145?assignee_username=broncato&&label_name[]=BTG-Project" class="btn btn-purple">Work Management Board</a>

- Work with the Finance and Accounting teams to improve workflows, processes, and application ecosystem
- Project work to implement new modules to tools
- Work with stakeholders from other teams like Field Ops and Fulfillment team that integration with the financial ecosystem.

##### In-Flight Projects
- Zuora Revenue Implementation
  - Operational Go-live date: December 18 2020.
  - Project development being tracked in [this Epic](https://gitlab.com/groups/gitlab-com/business-ops/-/epics/76).

- Zuora Health Check
  - Next Milestone: October 31 2020.
  - Project development and relevant information detailed in [the project wiki](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/wikis/1.-Zuora-Health-Check).

##### On Hold Projects
- Coupa
   - Start date: TBD
   - Project development will be tracked in [this Epic](https://gitlab.com/groups/gitlab-com/business-ops/-/epics/112).
- Zuora Collect
   - Start date: TBD
   - Project development is being tracked in [this Issue](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/325).

##### Completed Projects
- Adaptive Insights
  - Go-Live date: August 10 2020.
  - Project development and relevant information detailed in [the project wiki](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/wikis/2.-Adaptive-Insights).


##### Subscription app integrations and operations

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1586460?assignee_username=j.carey&" class="btn btn-purple">Work Management Board</a>

- Subscription App Analysis/Research, Documentation, and Architecture Recommendations
- Partner to Go to Market Operations (Marketing and Sales Ops), Sales Systems, Customer Success Operations, and Channel Operations.

### Other activies we support


##### Retrospectives

We can host your project retrospective.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

##### Application Evaluations

We provide templates for vendor evaluations, can help write and review your user stories, and are happy to participate in tool evaluations that integrate with other applications or intersect with multiple departments.

Please involve us in all tool evaluations that integrate into the enterprise application ecosystem before beginning demos with vendors.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

## Templates
*  [Rollout Plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=public-rollout-plan)
*  [Change Management: Third Party Applications Changes](https://gitlab.com/gitlab-com/business-ops/change-management/issues/new?issuable_template=Third%20Party%20Change%20Management)
*  [Change Management: Internal Tool Changes](https://gitlab.com/gitlab-com/business-ops/change-management/issues/new?issuable_template=Internal_Change_Management)
*  [Software application selection: Request for Proposal](https://docs.google.com/document/d/1_Q2b5opYUQ9TlGmF2vOJ6anu0spVFMkNO6YCR4UjYXM/edit?usp=sharing)
*  [Software application selection: User Stories](https://docs.google.com/spreadsheets/d/1c1R0pqKr8YwXXATzFVEUaofF2luNrHbmcNkKAWisebs/edit?usp=sharing)

##### Solving Business Problems/Architectural Troubleshooting

We have high level views of the enterprise application ecosystem and can help troubleshoot where a business process has broken down or a system flow is not working as expected.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

#####  Documentation we support

- [Tech Stack](/handbook/business-ops/tech-stack-applications/)
- [Subscription App Flow](/handbook/business-ops/enterprise-applications/applications/)
- [Change Management](/handbook/business-ops/business-technology-change-management/)



##### IT Operations and People Operations

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1596495?assignee_username=lisvinueza&" class="btn btn-purple">Work Management Board</a>

- Partner to Team Member Enablement and People Operations
- Business Technology Operations and Workflows

## <i class="fas fa-user-shield" id="biz-tech-icons"></i> IT Compliance Manager

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1802558?label_name[]=IT%20Compliance" class="btn btn-purple">Work Management Board</a>

- Partner to Legal and Security teams to ensure business compliance
    - Offboarding
    - GDPR/CCPA
    - Business Preparedness Plans
    - SOX Compliance

## <i class="fas fa-hand-holding-usd" id="biz-tech-icons"></i> Finance Operations

- Technical and Operational owner of the finance application ecosystem partnering with Finance and Accounting
- Maintains and optimizes the integrations of the ecosystem
- Partner to Sales Ops, Sales Systems, Growth Teams and other departments where the integrations intersect and the data passes from one system into another
- Slack Channel `#financesystems_help`

##### What we are working on?

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1722830?assignee_username=awestbrook&" class="btn btn-purple">Work Management Board</a>

- Backlog and In progress issues related to all [finance systems](/handbook/business-ops/enterprise-applications/#finance-systems-covered)

##### Types of Support

1. Access Request or change in access: [Queue](https://gitlab.com/groups/gitlab-com/-/boards/1765444?&label_name[]=FinSys%20-%20Access%20Request).
    Submit [issue](handbook/business-ops/employee-enablement/onboarding-access-requests/access-requests/).
1. Breaks, bugs and incidents related to a system.
    Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).
1. Enhancement Request for a system.
    Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).
1. Other and questions.
    Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).

##### Finance Systems Covered

1. Zuora: [Board](https://gitlab.com/groups/gitlab-com/-/boards/1723367?label_name[]=FinSys%20-%20Zuora) with everything slated to be done.
1. Netsuite
1. Tipalti
1. Expensify
1. Stripe
1. TripActions
1. Avalara
1. CaptivateIQ
1. Workiva
1. FloQast
1. Adaptive Planning

##### _Coming Soon_

1. Z-Revenue (RevPro)
1. Xactly
1. Mavenlink 
1. EdCast

#### What's the status of my request?

- Every issue will have a tag of either
- ~"BT PS:: Backlog" > Unless a due date is indicated or urgency specified, non-access related issues will go into the backlog and prioritized bi-weekly
- ~"BT PS::To Do" > Team will look at the issue within a week of submitting
- ~"BT PS::In Progress" > Team is currently actively workiing on scoping out and gathering requirements
- ~"BT PS::Done"

##### Change Process

1. [Issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new) submitted with request
1. Request is approved by technical owner and business owner (as necessary).
    ([Approvals Queue](https://gitlab.com/groups/gitlab-com/-/boards/1774935))
1. Change pushed to sandbox/dev environment (as necessary)
1. Change validated
1. Change depoloyed to production environment
