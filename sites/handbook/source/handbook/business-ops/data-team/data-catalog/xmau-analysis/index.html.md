---
layout: handbook-page-toc
title: "MAU/TMAU/Section MAU/SMAU/GMAU/AMAU Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## MAU/TMAU/Section MAU/SMAU/GMAU/AMAU Analysis

MAU/TMAU/Section MAU/SMAU/GMAU/AMAU is a single term to capture the various levels at which we capture Monthly Active Usage (MAU), encompassing Action (AMAU), Group (GMAU), Stage (SMAU), and Total (TMAU). In order to provide a useful single metric for product groups which maps well to product-wide Key Performance indicators, each MAU/TMAU/Section MAU/SMAU/GMAU/AMAU metric cascades upwards in the order noted above. 

MAU/TMAU/Section MAU/SMAU/GMAU/AMAU metrics are derived from Usage Ping (Self-Managed instance-level granularity) and GitLab.com (SaaS namespace-level granularity). This Analytics Workflow enables the analysis of each level of MAU/TMAU/Section MAU/SMAU/GMAU/AMAU metric across various segments of customers sets the foundation for reporting on [Reported, Estimated, and Projected](https://about.gitlab.com/handbook/product/performance-indicators/#three-versions-of-MAU/TMAU/Section MAU/SMAU/GMAU/AMAU) metrics.

### Knowledge Assessment & Certification

`Coming Soon`

### Data Classification

Some data supporting MAU/TMAU/Section MAU/SMAU/GMAU/AMAU Analysis is classified as [Orange](/handbook/engineering/security/data-classification-standard.html#orange) or [Yellow](/handbook/engineering/security/data-classification-standard.html#yellow). This includes ORANGE customer metadata from the account, contact data from Salesforce and Zuora and GitLab's Non public financial information, all of which shouldn't be publicly available. Care should be taken when sharing data from this dashboard to ensure that the detail stays within GitLab as an organization and that appropriate approvals are given for any external sharing. In addition, when working with row or record level customer metadata care should always be taken to avoid saving any data on personal devices or laptops. This data should remain in [Snowflake](/handbook/business-ops/data-team/platform/#data-warehouse) and [Sisense](/handbook/business-ops/data-team/platform/periscope/) and should ideally be shared only through those applications unless otherwise approved. 

**ORANGE**

- Description: Customer and Personal data at the row or record level.
- Objects:
  - `dim_billing_accounts`
  - `dim_crm_accounts`
  - `usage_ping_mart`

### Solution Ownership

- Source System Owner:
    - Versions: `@jeromezng`
    - Gitlab.com: `TBD`
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Source System Subject Matter Expert:
    - Versions: `@jeromezng`
    - Gitlab.com: `TBD`
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Data Team Subject Matter Expert: `@mpeychet_` `@m_walker`

### Key Terms

- [Account](/handbook/sales/#additional-customer-definitions-for-internal-reporting)
- Account Instances - the total number of reported instances that can be mapped to an account
- [Host](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)
- [Instance](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)
- Instance User Count - the total number of users on an instance
- [Paid User](/handbook/product/performance-indicators/#paid-user)
- [Product Tier](/handbook/marketing/product-marketing/tiers/#overview)
- [Usage Ping](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)
- [Version](/handbook/sales/process/version-check/#what-is-the-functionality-of-the-gitlab-version-check)

### Key Metrics, KPIs, and PIs

`Coming Soon`

## Self-Service Data Solution

### Self-Service Dashboard Viewer

| Dashboard                                                                                                    | Purpose |
| ------------------------------------------------------------------------------------------------------------ | ------- |
| Executive Overview - TBD | |
| Dev Section Analysis - TBD | |

### Self-Service Dashboard Developer

A great way to get started building charts in Sisense is to watch this 10 minute [Data Onboarding Video](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be) from Sisense. After you have built your dashboard, you will want to be able to easily find it again. Topics are a great way to organize dashboards in one place and find them easily. You can add a topic by clicking the `add to topics` icon in the top right of the dashboard. A dashboard can be added to more than one topic that it is relevant for. Some topics include Finance, Marketing, Sales, Product, Engineering, and Growth to name a few. 

#### Self-Service Dashboard Developer Certificate

`Coming Soon`

### Self-Service SQL Developer

#### Key Fields and Business Logic

`Coming Soon`

#### Self-Service SQL Developer Certificate

`Coming Soon`

#### Snippets

`Coming Soon`

#### Reference SQL

`Coming Soon`

#### Data Discovery Function in Sisense

If you are not familiar with SQL, there is the Data Discovery function in Sisense wherein you can create charts through a drag-and-drop interface and no SQL query is needed.

##### How to work with the Data Discovery Function

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/h_b9A8F7Ic8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

More information here on the [Data Discovery page in Sisense](https://dtdocs.sisense.com/article/data-discovery).

#### Entity Relationship Diagrams

`Coming Soon`

## Data Platform Solution

### Data Lineage

`Coming Soon`

### DBT Solution

`Coming Soon``

## Trusted Data Solution

[Trusted Data Framework](https://about.gitlab.com/handbook/business-ops/data-team/direction/trusted-data/)

### EDM Enterprise Dimensional Model Validations

`Coming Soon`

### RAW Source Data Pipeline validations

`Coming Soon`

### Manual Data Validations

`Coming Soon`

