---
layout: handbook-page-toc
title: "Verify:Testing Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision
We provide confidence in software through the delivery of meaningful, actionable automated testing results in GitLab.

## Mission
The Verify:Testing Group provides automated testing integration into GitLab. We aim to allow software teams to be able to easily integrate many layers of testing into their GitLab CI workflow including:

* Code Testing
* Code Quality
* Code Coverage
* Web performance testing
* Usability testing
* Accessibility testing

We want software teams to feel confident that the changes they introduce into their code are safe and conformant.

## Performance Indicators
We measure the value we contribute by using a [Product Performance Indicator](/handbook/product/metrics/). Our current PI for the Testing group is the [Paid GMAU](/handbook/product/ops-section-performance-indicators/#verifytesting---paid-gmau---count-of-active-paid-testing-feature-users). This is a rolling count of users who have interacted with a paid Testing feature.

### Usage Funnel
This funnel represents the customer journey and the various means a product manager may apply a Performance Indicator metric to drive a desired behavior in the funnel. This framework can be applied to any of the categories being worked on by Verify:Testing. The current priority is to increase Activation, Retention and Revenue within the [Code Testing and Coverage](/direction/verify/code_testing/) Category.

```mermaid
classDiagram
  Acquistion --|> Activation
    Acquistion: Awareness
    Acquistion: Measurement (# of page views on https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing)
  Activation --|> Retention
    Activation : Use
    Activation: Measurement (# of page views of Project Code Coverage Graph)
  Retention --|> Revenue
    Retention : Continued Use
    Retention: Measurement (# of projects that have viewed code coverage graph by day)
  Revenue --|> Referral
    Revenue : Payment
    Revenue: Measurement (# of projects that have viewed group code coverage graph by day)
  Referral --|> Acquistion
    Referral : Talk about
    Referral: Measurement (Insert Metric)
```

## Team Members
The following people are permanent members of the Verify:Testing group:

<%= shared_team_members(role_regexps: [/Verify(?!:)/, /Verify:Testing/]) %>

## Stable Counterparts
The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Verify/, direct_manager_role: 'Backend Engineering Manager, Verify:Testing', other_manager_roles: ['Frontend Engineering Manager, Verify (Interim)', 'Backend Engineering Manager, Verify:Continuous Integration', 'Backend Engineering Manager, Verify:Runner']) %>

#### JTBD
You can view and contribute to our current list of JTBD and job statements [here](/handbook/engineering/development/ops/verify/testing/JTBD/#jobs-to-be-done).

## Technologies
Like most GitLab backend teams we spend a lot of time working in Rails on the main [GitLab app](https://gitlab.com/gitlab-org/gitlab). Familiarity with Docker and Kubernetes is also useful on our team.

## Common Links
 * [Issue Tracker](https://gitlab.com/groups/gitlab-org/-/boards/364216?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Atesting)
 * [Slack Channel](https://gitlab.slack.com/archives/g_testing)
 * [Roadmap](https://about.gitlab.com/direction/ops/#verify)

## Our Repositories
* [GitLab](https://gitlab.com/gitlab-org/gitlab)
* [CodeQuality](https://gitlab.com/gitlab-org/ci-cd/codequality)
* [Accessibility](https://gitlab.com/gitlab-org/ci-cd/accessibility)

### Verify:Testing feature demo projects
* [Code Coverage](https://gitlab.com/gitlab-org/ci-cd/demos/coverage-report)

## Our active feature flags

Note: This table will be replaced with an issue filter at a later date.

| Flag name                            | Description                                                                                                                                                                           |
|:-------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `junit_pipeline_screenshots_view`    | [Docs](https://docs.gitlab.com/ee/ci/junit_test_reports.html#enabling-the-feature-1)                                                                                                  |
| `ci_limit_test_reports_size`         |                                                                                                                                                                                       |
| `coverage_report_view` | displays coverage report on merge request diff [Docs](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html)                                                                                                                     |
| `anonymous_visual_review_feedback`   | Allows comments to be posted to Merge Requests directly from Visual Review Tools without authentication [Docs](https://docs.gitlab.com/ee/ci/review_apps/#configuring-visual-reviews) |

## How we work

### Planning

#### Quad-planning
Also known as the [Three Amigos](ttps://www.agilealliance.org/glossary/three-amigos) process, Quad-planning is the development of a shared understanding about the scope of work in an issue and what it means for the work represented in an issue to be considered done. When an SET counterpart is assigned, we will perform Quad-planning earlier in the planning process than described in the [product workflow](https://about.gitlab.com/handbook/product-development-flow/#build-phase-1-plan), beginning when the PM develops initial acceptance criteria. Once this occurs, the PM will apply the `quad-planning:ready` label. At this point, asynchronous collaboration will begin between product, engineering, UX, and quality. Supplying these distinct viewpoints at this early stage of planning is valuable to our team. This can take place more than one milestone into the future. Once acceptance criteria is agreed upon, the `quad-planning:complete` label is applied.

#### Release planning issue
We use a [release planning issue](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/new?issuable_template=ReleasePlan) to plan our release-level priorities over each milestone. This issue is used to highlight deliverables, capacity, team member holidays, and more. This allows team members and managers to see at a high-level what we are planning on accomplishing for each release, and serves as a central location for collecting information.

#### Technical Investigation
In the process of refinement we may discover a new feature will require a [blueprint](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/doc/workflow.md) or the team feels input from maintainers will help scope down the problem, ensure the feature is performant and/or reduce future technical debt. When this happens the team will create a Technical Investigation issue for the investigation. This issue will be assigned to one team member, that team member should spend no more than 5 days of work time on the investigation (ideally 1 day). They will answer specific questions specified in the Technical Investigation issue before work on the feature is started. This process is analogous to the concept of a [Spike](https://about.gitlab.com/handbook/engineering/development/ops/monitor/#spike).

##### Measurement of Success
The Technical Investigation process is an *interim* process and as such is subject to achieving measurable success before it is confirmed as a long-term process. We will use this process on a trial basis for the next 2 technically challenging issues that the team faces. The measurement for this process will be gauging:

1. Engineer satisfaction with the process out of 10
1. Number of new issues created after work begins on the issues spawned from this investigation is less than 2

#### Release post checklist issue
During each milestone, we create a [Release Post Checklist](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/new?issuable_template=Release-Post-Issue) issue that is used by team members to help track the progress of the team [release posts](/handbook/marketing/blog/release-posts/). We use the checklist issue to associate release post merge requests with links to the implementation issue, links to updated documentation, and list which engineers are working on each issue. The checklist issue provides a single place to see all this information.

#### Deliverables in a Milestone
Engineering managers apply the `Deliverable` label on issues that meet the following criteria:

* It is delivering value that is visible to the user
* It is near the top of the issue board (high priority)
* The issue is well-defined
* The issue is weighted
* The engineering manager believes we have the capacity to complete the work in the assigned milestone
* The team has accepted the issue into the milestone and moved it to ~"workflow::ready for development" status

We will use the `Stretch` label if we feel the issue meets most of the criteria but either contains some known unknowns, or it's uncertain whether we will have the capacity to complete the work in the current milestone. If an issue with a `Stretch` label is carried over into the next milestone, its label will change to `Deliverable`. The position in the issue board is what confers priority, rather than the presence of a `Deliverable` label. However most high priority issues are `Deliverable`.

There may be some issues in a milestone with neither `Deliverable` nor `Stretch` labels but will strive to have the majority of issues labeled with one of these.

The team will trial this process for the 13.5 milestone and re-evaluate this labeling process.

#### Definition of Ready
Before the team will accept an issue into a milestone for work it must meet these criteria:

* Issues labeled with ~feature include a well stated "why" and customer problem
* Issues labeled ~bug include steps to reproduce
* Designs are in the design tab if needed

### Workflow
Unless specifically mentioned below, the Verify:Testing group follows the standard [engineering](/handbook/engineering/workflow/) and [product](/handbook/product-development-flow/) workflows.

#### Starting New Work
Verify:Testing team members are encouraged to start looking for work starting **_Right to left_** in the milestone board. This is also known as _"Pulling from the right"_. If there is an issue that a team member can help along on the board, they should do so instead of starting new work. This includes conducting code review on issues that the team member may not be assigned to if they feel that they can add value and help move the issue along the board.

Specifically this means, in order:
* Doing verification that code has made it to staging, canary, or production
* Conducting code reviews on issues that are `workflow::in review`
* Unblocking anyone in the `workflow::in development` column
* Then, finally, picking from the top of the `workflow::ready for development` column OR an item the team member investigated to apply the estimated weight if unfamiliar with the top item

The goal with this process is to reduce WIP. Reducing WIP forces us to "Start less, finish more", and it also reduces cycle time. Engineers should keep in mind that the DRI for a merge request is the **author(s)**, just because we are putting emphasis on the importance of teamwork does not mean we should dilute the fact that having a [DRI is encouraged by our values](/handbook/people-group/directly-responsible-individuals/#dris-and-our-values).

#### Code Review
Code reviews follow the standard process of using the reviewer roulette to choose a reviewer and a maintainer. The roulette is **optional**, so if a merge request contains changes that someone outside our group may not fully understand in depth, it is encouraged that a member of the Verify:Testing team be chosen for the preliminary review to focus on correctly solving the problem. The intent is to leave this choice to the discretion of the engineer but raise the idea that fellow Verify:Testing team members will sometimes be best able to understand the implications of the features we are implementing. The maintainer review will then be more focused on quality and code standards.

We also recommend that team members take some time to review each others merge requests even if they are not assigned to do so, as described in the [GitLab code review process](/handbook/engineering/workflow/code-review/#reviewer). It is not necessary to assign anyone except the initial domain reviewer to your Merge Request. This process augmentation is intended to encourage team members to review Merge Requests that they are not assigned to. As a new team, reviewing each others merge requests allows us to build familiarity with our product area, helps reduce the amount of investigation that needs to be done when implementing features and fixes, and increases our [lottery factor](https://en.wikipedia.org/wiki/Bus_factor). The more review we can do ourselves, the less work the maintainer will have to do to get the merge request into good shape.

This tactic also creates an environment to ask for early review on a WIP merge request where the solution might be better refined through collaboration and also allows us to share knowledge across the team.

#### Issue Health Status Definitions:
- **On Track** - We are confident this issue will be completed and live for the current milestone. It is all [downhill from here](https://basecamp.com/shapeup/3.4-chapter-12#work-is-like-a-hill).
- **Needs Attention** - There are concerns, new complexity, or unanswered questions that if left unattended will result in the issue missing its targeted release. Collaboration needed to get back `On Track` within the week.
   - If you are moving an item into this status please mention individuals in the issue you believe can help out in order to unstick the item so that it can get back to an `On Track` status.
- **At Risk** - The issue in its current state will not make the planned release and immediate action is needed to get it back to `On Track` today.
  - If you are moving an item into this status please consider posting in the [https://gitlab.slack.com/archives/CPANF553J](#g_testing) channel in slack. Try to include anything that can be done to unstick the item so that it can get back to an `On Track` status in your message.
  - Note: It is possible that there is nothing to be done that can get the item back on track in the current milestone. If that is the case please let your manager know as soon as you are aware of this.

#### Issue progress updates
When an engineer is actively working (workflow of ~workflow::"In dev" or further right on current milestone) on an issue they will periodically leave status updates as top-level comments in the issue. The status comment should include the updated health status, any blockers, notes on what was done, if review has started, and anything else the engineer feels is beneficial. If there are multiple people working on it also include whether this is a front end or back end update. An update for each of MR associated with the issue should be included in the update comment. Engineers should also update the [health status](https://docs.gitlab.com/ee/user/project/issues/#health-status) of the issue at this time.

This update need not adhere to a particular format. Some ideas for formats:

```text
Health status: (On track|Needs attention|At risk)
Notes: (Share what needs to be shared specially when the issue needs attention or is at risk)
```

```text
Health status: (On track|Needs attention|At risk)
What's left to be done:
What's blocking: (probably empty when on track)
```

```text
## Update <date>
Health status: (On track|Needs attention|At risk)
What's left to be done:

#### MRs
1. !MyMR1
1. !MyMR2
1. !MyMR3
```

There are several benefits to this approach:

* Team members can better identify what they can do to help the issue move along the board
* Creates an opening for other engineers to engage and collaborate if they have ideas
* Leaving a status update is a good prompt to ask questions and start a discussion
* The wider GitLab community can more easily follow along with product development
* A history of the roadblocks the issue encountered is readily available in case of retrospection
* Product and Engineering managers are more easily able to keep informed of the progress of work

Some notes/suggestions:

* We typically expect engineers to leave at least one status update per week, barring special circumstances
* Ideally status updates are made at a logical part of an engineers workflow, to minimize disruption
* It is not necessary that the updates happen at the same time/day each week
* Generally when there is a logical time to leave an update, that is the best time
* Engineers are encouraged to use these updates as a place to collect some technical notes and thoughts or "think out loud" as they work through an issue

#### Monitoring changes behind feature flags
In addition to the steps documented for [developing with feature flags at GitLab](https://docs.gitlab.com/ee/development/feature_flags/) Verify:Testing engineers monitor their changes' impact on infrastructure using dashboards and logs where possible. Because feature flags allow engineers to have complete control over their code in production it also enables them to take ownership of monitoring the impact their changes have against production infrastructure. In order to monitor our changes we use this [helpful selection of dashboards](https://about.gitlab.com/handbook/engineering/monitoring/#selection-of-useful-dashboards-from-the-monitoring) and specifically the [Rails controller dashboard](https://dashboards.gitlab.net/d/web-rails-controller/web-rails-controller) (Internal Only) for monitoring our changes in production. Metrics we evaluate include latency, throughput, CPU usage, memory usage, and database calls, depending on what our change's expected impact will be and any considerations called out in the issue.

The goal of this process is to reduce the time that a change could potentially have an impact on production infrastructure to the smallest possible window. A side benefit of this process is to increase engineer familiarity with our monitoring tools, and develop more experience with predicting the outcomes of changes as they relate to infrastructure metrics.

#### Closing Issues
After an engineer has ensured that the [Definition of Done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done) is met for an issue, they are the ones responsible for closing it. The engineer responsible for verifying an issue is done is the engineer who is the DRI for that issue, or the DRI for the final merge request that completes the work on an issue.

### Meetings
We meet on a weekly cadence for two separate meetings. Typically we conduct both of these meetings sequentially.

#### Testing Group Weekly Meeting
This synchronous meeting is to discuss anything that is blocking, or notable from the past week. This meeting acts as a team touchpoint.

#### Testing Group Refinement Meeting
This synchronous meeting is to refine and triage the current and next milestone's issues. In this meeting we should be: discussing priority, sizing issues, estimating the weight of issues, keeping track of our milestone bandwidth and removing issues appropriately.

#### Async Daily Standups
We use [geekbot](https://geekbot.com/) integrated with Slack for our daily async standup. The purpose of the daily standup meeting is to keep the team informed about what everyone is working on, and to surface blockers so we can eliminate them. The standup bot will run at 10am in the team members local time and ask 2 questions:
1. What will you do today?
1. Anything blocking your progress?

#### Async Monthly Retrospectives
We use a GitLab issue in [this project](https://gitlab.com/gl-retrospectives/verify-stage/testing/-/issues/) for our monthly retrospective. The issue is created automatically towards the end of the current milestone. The purpose of the monthly retrospective issue is to reflect on the milestone and talk about what went well, what didn't go so well, and what we can do better.

#### Think BIG / Think small
We have a monthly synchronous 30-minute [think big meeting](https://about.gitlab.com/handbook/engineering/ux/thinkbig/), followed the next week by a monthly 30-minute think small meeting on the same topic of the previous think big meeting. This pair of meetings is modeled after the Gitlab Product Manager [deep dive interview](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Product/DeepDive.md). The purpose of this meeting is to discuss the vision, product roadmap, user research, design, and delivery around the Testing features. The goal of this meeting will be to align the team on our medium to long-term goals and ensure that our short-term goals are leading us in that direction. This meeting is useful for aligning the team with its stable counterparts and ensuring that engineers have an understanding of the big picture and so they know how their work fits into the long-term goals of the team.

## How to work with us

### On issues
Issues worked on by the testing group have a group label of ~"group::testing". Issues that contribute to the verify stage of the devops toolchain have the ~"devops::verify" label.
