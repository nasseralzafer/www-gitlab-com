---
layout: handbook-page-toc
title: "Leadership"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page contains leadership pointers.
The first couple of headers indicate which group they apply to, using the groupings
defined on our [team structure page](/company/team/structure/).

## GitLab team members

1. At GitLab, leadership is requested from everyone, whether an individual contributor or member of the leadership team.
1. As a leader, GitLab team members will follow your behavior, so always do the right thing.
1. Everyone who joins GitLab should consider themselves ambassadors of our [values](/handbook/values) and protectors of our [culture](/company/culture/).
1. Behavior should be consistent inside and outside the company, just do the right thing inside the company, and don't fake it outside.
1. GitLab respects your judgment of what is best for you, since you know yourself best. If you have a better opportunity somewhere else don't stay at GitLab out of a sense of loyalty to the company.
1. In tough times people will put in their best effort when they are doing it for each other.
1. We work [asynchronously](/company/culture/all-remote/management/#asynchronous). Lead by example and make sure people understand that [things need to be written down in issues as they happen](/company/culture/all-remote/self-service/#paying-it-forward).
1. We are not a democratic or consensus driven company. People are encouraged to give their comments and opinions, but in the end [one person decides](/handbook/people-group/directly-responsible-individuals/) the matter after they have listened to all the feedback.
1. It is encouraged to disagree and have constructive debates but please [argue intelligently](https://www.brainpickings.org/2014/03/28/daniel-dennett-rapoport-rules-criticism/).
1. We value truth seeking over cohesion.
1. We avoid [meetings](/company/culture/all-remote/meetings/), when possible, because they don't support the asynchronous work flow and are hard to conduct due to timezone differences.
1. Start meetings on time, be on time yourself, don't ask if everyone is there, and don't punish people that have shown up on time by waiting for people or repeating things for those that come late. When a meeting unblocks a process or decision, don't celebrate that but instead address the question: How can we unblock in the future without needing a meeting?
1. We give [feedback](/company/culture/all-remote/effective-communication/#feedback-is-a-gift), lots of it. Don't hold back on suggestions for improvements.
1. If you meet external people, always ask what they think we should improve.
1. Following from [Paul Graham's advice](https://twitter.com/paulg/status/802102152319606784): Strive to make the organization simpler.
1. Saying something to the effect of "as you might have heard", "unless you've been living in a cage you know", "as everyone knows", or "as you might know" is toxic. The people that know don't need it to be said. The people that don't know feel like they missed something and might be afraid to ask about the context.
1. [Don't use someone else's name](https://twitter.com/emiliejayg/status/1198731054162432000?s=12), remind people of your title, or otherwise ["pull rank"](/handbook/values/#dont-pull-rank) to get things done.


## Interim and Acting Leadership
In some cases, a individual in the [Management group](/company/team/structure#management-group), [Director group](/company/team/structure#director-group), [S-group](/company/team/structure#s-group), or even [E-group](/company/team/structure#e-group) may have an "Interim" or "Acting" title.
1. **Acting** means that someone is occupying this role temporarily and will move back to their original role after a set amount of time or other conditions, such as an external hire.
1. **Interim** means the individual is working on a promotion into the role.

In either case, they will be fulfilling the full responsibilities of the role. If you have any questions, about the future of the role, please ask them or their manager.

Individual departments will have their own criteria for who is eligible to occupy these roles, so please check the career development page for your department.

## Making decisions

1. We use our [values](/handbook/values/), and particularly our [values hierarchy](/handbook/values/#hierarchy), to guide the decisions we make.
1. We combine the [best of both hierarchical and consensus organizations](/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making). Hierarchical organizations have good speed but are bad at gathering data, leading to people saying yes but not doing it. Consensus organizations are good at gathering data but lack speed, leading to projects happening under the radar. We split decisions into two phases. The data gathering phase has the best of consensus organizations, where everyone can contribute. The decision phase has the best of a hierarchical organization, the person that does the work or their manager decides what to do.
1. If you apply consensus in both the data gathering phase and the decision phase you lose speed and you get decisions that try to stay under the radar so there are fewer people to convince.
1. If you apply hierarchy in both the data gathering phase and the decision phase you lose valuable input.
1. Providing input but then not being part of the decision making phase is counterintuitive, you feel ignored. We'll have to accept that people listened to us but don't owe us an explanation to have fast decisions based on everyone's input.
1. At GitLab, decision making is based on an informed and knowledgeable hierarchy, not on consensus or democracy. Voting on material decisions shows a lack of informed leadership.
1. Make data driven decisions but consider environments that do not allow reliable data collection. According to [research by the Harvard Business Review](https://hbr.org/2016/02/the-rise-of-data-driven-decision-making-is-real-but-uneven), "experience and knowledge of leaders in the subject matter still outperforms purely data-driven approaches."
1. When analyzing trends, never show cumulative graphs because they always look up and to the right even if business is bad.
1. Be aware of your unconscious biases and emotional triggers.
1. We don't have project managers. Individual contributors need to manage themselves. Not everyone will be able to do this effectively and fit our organization. Making someone responsible for managing others will cause negative effects to the results of the people that can manage themselves. If you manage yourself you have a much greater freedom to make decisions, and those decisions are based on deep knowledge of the situation. We want to retain the people that can handle that responsibility and therefore we can't retain the ones that struggle. Assigning a project manager/coordinator/case manager/etc. to something is an indicator that something is wrong and we are picking the wrong solution. The notable exception to this is in the [Professional Services](/services/) organization. While most functions at GitLab are serving the product or the company, ProServe is a services company which collaborates closely with customers and is sometimes contractually obligated to have project managers.
1. The person that does the work makes the decisions, they are the [Directly Responsible Individual (DRI)](/handbook/people-group/directly-responsible-individuals/). They should listen to the data and informed opinions of others to arrive at their decision. Part of making good decisions is knowing who has good information and/or experience that informs that decision. Once a DRI has made a decision, [Disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree)
1. A DRI may make a decision that results in (and is hence the cause of) negative feelings, but it is important to remember [Collaboration is not consensus](/handbook/values/#collaboration-is-not-consensus) and [People are not their work](/handbook/values/#people-are-not-their-work). While others are invited to contribute data and informed opinions during the decision making process, the DRI is not responsible for how they feel. When contributing supporting information to making a decision, it is the responsibility of the contributors to do so with data, use cases, historic examples, etc and not personal opinions or attacks.
1. Short way to phrase this: We can allow others into our kitchen because we can always send them out (inviting people to give input is much easier if you retain the ability to make a decision by yourself).

> If good decision-making appears complicated, that’s because it is and has been for a long time. Let me quote from Alfred Sloan, who spent a lifetime preoccupied with decision-making: “Group decisions do not always come easily. There is a strong temptation for the leading officers to make decisions themselves without the sometimes onerous process of discussion.”
> - _Chapter 5: Decisions, Decisions of High Output Management by Andy Grove_

## Communication should be direct, not hierarchical

Most companies communicate from top to bottom through a chain of command. This communication flow often empowers managers, but it also introduces inefficiency as team members are not able to connect directly with the people they need to communicate with in order to get their work done. At GitLab, every team member is encouraged to reach out to whoever is the correct person (or people) to quickly unblock issues, solve problems or support in other ways. Do be courteous of your direct manager and copy them on the request. We don't encourage unnecessary friction in asking team members to escalate through managers and wait for responses to come back. What matters is efficiency in getting to results. Slack the CEO, Slack a VP, or Slack a peer. Do what you need to do to make GitLab successful.

Managers should not be bottlenecks or silos for communication.
Anyone should feel [comfortable](/handbook/values/#short-toes) reaching out to anyone else with the best information they can to solve a problem.
This is a more [efficient](/handbook/values/#efficiency), [transparent](/handbook/values/#transparency), and [collaborative](/handbook/values/#collaboration) way to work.

## Giving Feedback

Giving regular [feedback](/company/culture/all-remote/effective-communication/#feedback-is-a-gift) is extremely important for both managers and team members. Feedback can take the form of coaching sessions, separate from [1-on-1 meetings](/handbook/leadership/1-1). Giving feedback is also about being prepared and, depending on the situation, you should create separate agendas and structure them as follows:

- Provide [context](/company/culture/all-remote/effective-communication/#understanding-low-context-communication)
- Use a framework for your feedback. Examples: 1) Two Areas: [Praise](/company/culture/all-remote/effective-communication/#feedback-is-a-gift) (What's working well) and Tips (What could be done differently) or 2), Three Areas: Start, Stop, Continue
- Ask yourself, is this:
  - Actionable
  - Specific
  - [Kind](/handbook/values/#kindness) (Does the feedback help the person? Note: Being kind is not the same as being nice.)
  - Objective (similar to Fair)
  - Relevant to the job role and [compa ratio](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio)

  Ask the questions listed in the [1-on-1 guide](/handbook/leadership/1-1) and the [career development discussion at the 1-on-1](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) section.

### Identifying root causes

Sometimes when performance dips, the best way to tackle it is to try to determine the root cause. This is easier said than done. There is a great tool that [CEB (now Gartner)](https://www.cebglobal.com/) created to help with this called [performance issue root cause diagnostic](https://offices.depaul.edu/human-resources/employee-relations/Documents/Performance%20Issue%20Root%20Cause%20Diagnostic.pdf). It may not always be possible or appropriate to determine the root cause, so the [underperformance process](/handbook/underperformance/) should be followed.
## Responding to Negative Feedback

As a leader, the way you respond to negative [feedback](/company/culture/all-remote/effective-communication/#feedback-is-a-gift) makes a significant impact
on your team. Remember that it can be difficult for people to approach someone
in authority with concerns and respond with sensitivity and appreciation. In particular, we
recommend that you keep the following in mind:

- Don't argue or get defensive. Accept the feedback for what it is: an attempt
  to help you improve your work or your professional relationships. If you do
  have to explain yourself, try to remain empathetic.
- It's fine (even preferable) to defer action. When presented with negative
  feedback, we often feel like we have to either justify our actions or promise
  change, and since change isn't always easy when you're responsible for a
  large team, justification becomes the default. It's OK to say you need time to
  evaluate the feedback and decide how to proceed.
- [The Right Way to Respond to Negative
  Feedback](https://hbr.org/2018/05/the-right-way-to-respond-to-negative-feedback)
- If a team member from your department or another part of the org comes to you and says they do not feel like they or their reports' contributions are valued by your reports, the manager should try to resolve this. Research shows that this is more likely to happen to underrepresented minorities. Please note that [DRIs are free to ignore feedback](/handbook/people-group/directly-responsible-individuals/) without acknowledging it and that [valuing contributions isn't the same as agreeing with them](/handbook/values/#collaboration-is-not-consensus). This is about co-opting someone else's idea without attribution and/or dismissing an idea with an ad-hominem remark.

## 1-on-1

Please see [/handbook/leadership/1-1](/handbook/leadership/1-1).

## Skip level interactions

Please see [/handbook/leadership/skip-levels](/handbook/leadership/skip-levels).

## Managers of one

In an all-remote organization, we want each team member to be a [manager of one](https://signalvnoise.com/posts/1430-hire-managers-of-one). A [manager of one](/handbook/values/#managers-of-one) is an attribute associated with our [Efficency value](/handbook/values/#efficiency). To be successful at GitLab, team members need to develop their daily priorities to achieve goals. Managers of one set the tone for their work, assign items and determine what needs to get done. No matter what role you serve, self-leadership is an essential skill needed to be successful as a manager of one.

**Skills and behavior of being a manager of one as a Team Member**:
*  Takes responsibilty to complete tasks and goals within appropriate timelines
*  Delivers on commitments independently and without supervision
*  Drives issue-based discussions grounded in a clear understanding of challenges and barriers
*  Highlights areas for process improvement and proactively brings them up with leadership
*  Continuously looks for opportunities to improve or iterate on current processes
*  Communicates status of goals and delivers on agreed-upon timing of completion with leadership

**Skills and behaviors of being a manager of one as a People Leader**:
*  Holds their team accountable for establishing goals and meeting their commitments
*  Consistently hires managers of one that fit the [GitLab values](/handbook/values/)
*  Establishes goals for their team while seeking input from leadership
*  Skilled at having difficult conversations with team members and leadership
*  Strengthens team relationships using empathy and [emotional intelligence](/handbook/people-group/learning-and-development/emotional-intelligence/) to adapt as needed to enable the manager of one skills and behaviors
*  Serves as a role model for what it takes to be successful as a manager of one in an all-remote setting

## No matrix organization

1. We believe everyone deserves to report to exactly one person that knows and understands what you do day to day. [The benefit of having a technically competent manager is easily the largest positive influence on a typical worker’s level of job satisfaction.](https://hbr.org/2016/12/if-your-boss-could-do-your-job-youre-more-likely-to-be-happy-at-work) We have a simple functional hierarchy, everyone has one manager that is experienced in their subject matter. Matrix organizations or [dotted lines](https://www.global-integration.com/glossary/dotted-line-reporting/) are too hard to get right.
1. We don't want a matrix organization where you work with a lead day to day but formally report to someone else.
1. The advantage of a functional structure is that you get better feedback and training since your manager understands your work better than a general manager.
1. For the organization, forgoing a separate class of managers ensures a simple structure with clear responsibilities.
1. A functional organization structure mimics the top structure of our organizations (Finance, Sales, Engineering, etc.).
1. It reduces compensation costs, coordination costs, and office politics.
1. The disadvantage is that your manager has a limited amount of time for you and probably has less experience managing people.
1. To mitigate these disadvantages we should offer ample training, coaching, support structures, and processes to ensure our managers can handle these tasks correctly and in a limited amount of time.
1. Everyone deserves a great manager that helps them with their career. A manager should hire a great team, should let you know when to improve, motivate and coach you to get the best out of you.
1. "Nuke all matrices. Nuke all dual reporting structures. And nuke as many shared services functions as you possibly can." from the great [guide to big companies from Marc Andreessen](http://pmarchive.com/guide_to_big_companies_part2.html) (the other guides are awesome too).
1. We recommend reading [High Output Management](/handbook/leadership/#books), and its author coined Grove's law: All large organizations with a common business purpose end up in a hybrid organizational form. We believe a dual reporting structure is inevitable, we just want to delay it as long as possible.
1. We do make features with a [DevOps stage group](/company/team/structure/#groups) that is a collection of teams and [stable counterparts](/company/team/structure/#specialist).
1. Whenever there is need to work on a specific, high-level, cross functional business problem, we can assemble a [working group](/company/team/structure/working-groups/).
1. Functional companies are easier when you focus on one product. Apple focuses on the iPhone and can have a [unitary/functional/integrated organizational form](https://stratechery.com/2016/apples-organizational-crossroads/). The advantage is that you can make one strong integrated product. We can also maintain a functional organization as long as we keep offering new functionality as features of GitLab instead of different products. The fact that we're in touch with the market because we use our own product helps as well.
1. Having functional managers means that they are rarely spending 100% of their time managing. They always get their hands dirty. Apart from giving them relevant experience, it also focuses them on the output function more than the process. Hopefully both the focus and not having a lot of time for process reduces the amount of politics.

## Stable counterparts

We want to promote organic cross-functional collaboration by giving people stable counterparts for other functions they need to work with. For example, each Strategic Account Leader (SAL) works with one Sales Development Representative (SDR). With our [categories](/handbook/product/categories/) every backend team of developers maps to a [Product Manager (PM)](/job-families/product/product-manager/) and a [frontend team](/handbook/engineering/frontend/#teams).

Giving people a **stable counterpart** allows for more social trust and familiarity, which speeds up decision making, prevents communication problems, and reduces the risk of conflicts. This way we can work effectively cross functionally without the [downsides of a matrix organization](#no-matrix-organization).

## Factory vs. studio

We want the best combination of [a factory and a studio](https://medium.com/@mcgd/factory-vs-studio-fb83b3fe9e14). The studio element means anyone can chime in about anything, from a user to the CEO. You can step outside your work area and contribute. The factory element means everyone has a clearly assigned task and authority.

## Process gets a bad rep

Process has a bad reputation. It has that reputation for things that we try to avoid doing at GitLab. When you have processes that are not needed it turns into a bureaucracy. A good example are approval processes. We should keep approval processes to a minimum, by both giving people the authority to make decisions by themselves and by having a quick lightweight approval process where needed.

But process also has good aspects. Having a documented process for how to communicate within the company greatly reduces time spend on on-boarding, increases speed, and prevents mistakes. A counterintuitive effect is that it also makes it easier to change processes. It is really hard to change a process that doesn't have a name or location and lives in different versions in the heads of people. Changing a written process and distributing the [diff](https://en.wikipedia.org/wiki/Diff_utility#Usage) is much easier.

## Recruiting and retention

Managers have an tremendous responsibility around recruiting and [retention](/handbook/people-group/people-group-metrics/#team-member-retention) of team members.

* Voluntary departures should be low, especially unexpected ones. The most common reasons for resignations can be tied back to the manager.
* We want few candidates to decline an offer, especially when the reason isn't compensation.
* We need adequate candidate pipeline volume and quality, especially for crucial positions.
* Candidates that have a proposed offer should meet the bar, especially for more senior positions.
* Build a _global team_. Unless shown with a business case, "we can’t find the talent out of the bay" goes against our [diversity, inclusion and belonging mission](/company/culture/inclusion/#diversity-inclusion--belonging-mission-at-gitlab) and the [Location Factor KPI](/handbook/people-group/people-group-metrics/#average-location-factor).

## Building High Performing Teams

Building a team to deliver [results](/handbook/values/#results) is a very important aspect of improving [efficiency](/handbook/values/#efficiency) and [iteration](/handbook/values/#iteration). A high-performing team will always deliver results. As a leader at GitLab, your role is to develop a high-performing team to reach the desired level of performance and productivity. There are certain traits that high-performing teams display at GitLab:

- Have a clear vision of their objectives and goals
- Stay committed to achieving their goals
- Manage conflicts
- Maintain effective communication and a healthy relationship with each other
- Make unanimous decisions as a team

**Skills and behavior of [building high performing teams competency](/handbook/competencies/#list) for Managers**:

- Models and encourages teamwork by fostering collaboration, communication, trust, shared goals, mutual accountability and support
- Fosters an environment where results are balanced with time management of multiple assignments and DRI's on important topics
- Empowers team members to be a Manager of One and gives them the tools to grow professionally in their careers
- Attracts and retains top talent by creating an inclusive environment built on trust, delegation, accountability, and teachability

### Strategies to Build High Performing Teams

The [Drexler-Sibbet Team Performance Model](https://www.kaizenko.com/drexler-sibbet-team-performance-model/) is an excellent tool to help [build high performing teams](/handbook/people-group/learning-and-development/building-high-performing-teams/) at GitLab. The model provides a roadmap for a team and a common language. It is a simplified description of how a team works together that highlights the most important things the team needs to focus on to reach high performance. At GitLab, we can use it as a frame of reference to developing high performing teams. It can help Managers ensure new and existing team members know the mission and direction of the team by the following:
- To form your team
- To guide what your team does
- To monitor how well your team is doing
- to diagnose where your team may be struggling or identify the keys to your team's success

**7 Stages to developing high performing teams:**
1. Orientation - Why are we here? Team members need to see a sense of team identity and how individual team members fit in.
2. Trust Building - Who are you? Team members share mutual regard for each other and are open and supportive of trust-based relationships.
3. Goal Clarification - What are we doing? Assumptions are made clear; individual assumptions are made known with a clear vision of the end state.
4. Commitment - How will we do it? Team members understand how it will make decisions and do the work.
5. Implementation - Who does what, when, where? Team members have a sense of clarity and can operate effectively due to the alignment of shared goals.
6. High Performance - Wow! The team is accomplishing more than it expected. The team has taken off, creativity is fostered and goals are surpassed.
7. Renewal - Why continue? The team is given recognition and celebrates achievements of individuals that produce valuable work. Reflect on lessons learned and reassess for the future.


## Articles

1. [Carta's Manager’s FAQ](https://readthink.com/a-managers-faq-35858a229f84)
1. [Carta's How to hire](https://carta.com/blog/how-to-hire/)
1. [How Facebook Tries to Prevent Office Politics](https://hbr.org/2016/06/how-facebook-tries-to-prevent-office-politics)
1. [The Management Myth](http://www.theatlantic.com/magazine/archive/2006/06/the-management-myth/304883/)
1. [Later Stage Advice for Startups](http://themacro.com/articles/2016/07/later-stage-advice-for-startups/)
1. [Mental Models I Find Repeatedly Useful](https://medium.com/@yegg/mental-models-i-find-repeatedly-useful-936f1cc405d)
1. [This Is The Most Difficult Skill For CEOs To Learn](http://www.businessinsider.com/whats-the-most-difficult-ceo-skill-managing-your-own-psychology-2011-4)
1. Great article about [how to think about PIPs](https://mfbt.ca/how-i-talk-to-leaders-about-firing-people-8149dfcb035b), although our time scales are shorter.
1. [Impraise Blog: 1-on-1s for Engaged Employees](https://www.impraise.com/blog/1-on-1s-for-engaged-employees-how-good-managers-should-do-them)
1. [Mind Tools: Giving Feedback: Keeping Team Member Performance High, and Well Integrated](https://www.mindtools.com/pages/article/newTMM_98.htm)
1. [Remote.Co: 5 Tips for Providing Feedback to Remote Workers](https://remote.co/5-tips-for-providing-feedback-to-remote-workers/)
1. [Really interesting blog post from Hanno on remote team feedback](https://hanno.co/blog/remote-team-feedback/)
1. [51 questions to ask in one-on-ones with a manager](https://getlighthouse.com/blog/questions-ask-one-on-ones-manager/)
1. [HBR: The rise of data driven decision making is real but uneven](https://hbr.org/2016/02/the-rise-of-data-driven-decision-making-is-real-but-uneven)
1. [Forbes: 6 Tips for Making Better Decisions](https://www.forbes.com/sites/mikemyatt/2012/03/28/6-tips-for-making-better-decisions/#966eb3b34dca)

## Books

Books in this section [can be expensed](/handbook/spending-company-money).

Notable books from the [E-Group Offsite Book Selections](/handbook/ceo/offsite/#previous-reads) may be added to the list below.

We sometimes self-organize [book clubs](/handbook/leadership/book-clubs) to read through these books as a group.

1. [High Output Management](https://www.goodreads.com/book/show/324750.High_Output_Management) - Andrew Grove
    * [Top 10 quotes](https://getlighthouse.com/blog/andy-grove-quotes-leadership-high-output-management/)
    * [Book club](/handbook/leadership/book-clubs/#high-output-management)
1. [The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers](https://www.goodreads.com/book/show/18176747-the-hard-thing-about-hard-things) - Ben Horowitz
    * [Chase Wright's notes](https://docs.google.com/document/d/1Uxva11x1YX4zci1FHmF45UTYjGLPMU5HmrbmflINoG4/)
1. [The score takes care of itself - Bill Walsh - PDF](http://coachjacksonspages.com/The%20Score%20Takes%20Care.pdf)
1. [Crucial Conversations: Tools for Talking When Stakes Are High](https://www.goodreads.com/book/show/15014.Crucial_Conversations) - Kerry Patterson
    * Notes from the [E-group](/company/team/structure#e-group) reading:
        * Virtual teams are much more likely to fail on crucial conversations than colocated teams
        * We need to develop the skill of sensing the tone of a-sync conversations to uncover potential issues
        * We need to find a way to create psychological safety for people in official channels
        * Starting with empathy is a great way to gather the context needed in a tense situation - this is hard a-sync, but more important
        * Consider getting context 1-on-1 (through Slack) before posting a comment in an issue that you might regret later
        * As leaders, we need to give context as well. A good question is: "What would have to change for us to get X prioritized..."
        * Documenting something is *not* a replacement for having the hard conversation
    * [Book club](/handbook/leadership/book-clubs/#crucial-conversations)
1. [The Advantage: Why Organization Health Trumps Everything Else In Business](https://www.goodreads.com/book/show/12975375-the-advantage) - Patrick Lencioni
1. [The Five Dysfunctions of a Team: A Leadership Fable](https://www.goodreads.com/book/show/21343.The_Five_Dysfunctions_of_a_Team) - Patrick Lencioni
1. [Crossing the Chasm: Marketing and Selling High-Tech Products to Mainstream Customers](https://www.goodreads.com/book/show/61329.Crossing_the_Chasm) - Geoffrey A. Moore
1. [The First 90 Days: Proven Strategies for Getting Up to Speed Faster and Smarter](https://www.goodreads.com/book/show/15824358-the-first-90-days) - Michael D. Watkins
1. [The 21 Irrefutable Laws of Leadership: Follow Them and People Will Follow You](https://www.goodreads.com/book/show/815716.The_21_Irrefutable_Laws_of_Leadership) - John C. Maxwell
1. [Thinking, Fast and Slow](https://www.goodreads.com/book/show/11468377-thinking-fast-and-slow) - Daniel Kahneman
1. [The Power of Habit](https://www.goodreads.com/book/show/12609433-the-power-of-habit) - Charles Duhigg
1. [Your Brain at Work](https://www.goodreads.com/book/show/6899290-your-brain-at-work) - David Rock
1. [Start with Why](https://www.goodreads.com/book/show/7108725-start-with-why) - Simon Sinek
1. [Leaders Eat Last](https://www.goodreads.com/book/show/16144853-leaders-eat-last) - Simon Sinek
1. [How to Win Friends & Influence People](https://www.goodreads.com/book/show/4865.How_to_Win_Friends_and_Influence_People) - Dale Carnegie
1. [How Google Works](https://www.goodreads.com/book/show/23158207-how-google-works) - Eric Schmidt
1. [Good to Great](https://www.goodreads.com/book/show/76865.Good_to_Great) - James C. Collins
1. [The Last Lecture](https://www.goodreads.com/book/show/40611510-the-last-lecture) - Randy Pausch
1. [Mastery](https://www.goodreads.com/book/show/13589182-mastery) - Robert Greene
1. [Radical Candor](https://www.goodreads.com/book/show/48430205-radical-candor) - Kim Malone Scott
1. [Creativity, Inc](https://www.goodreads.com/book/show/18077903-creativity-inc) - Ed Catmull, Amy Wallace
1. [Turn the Ship Around!](https://www.goodreads.com/book/show/16158601-turn-the-ship-around) - L. David Marquet

## Email Lists

1. [Software Lead Weekly](http://softwareleadweekly.com/)
1. [Threatpost Security Newsletter](https://threatpost.com/) (Subscribe at the bottom of the page)

## Training

When you give leadership training please [screenshare the handbook instead of creating a presentation](/handbook/handbook-usage/#screenshot-the-handbook-instead-of-creating-a-presentation).

## People Group

Feel free to reach out to anyone in the [People Group](https://about.gitlab.com/handbook/people-group/) for further support on leadership development topics. You can find us on the [team page](/company/team/), using the `People Group` dropdown. The team may also be reached in the `#peopleops` chat channel.
 
## Being a public company

Learn more on GitLab's view of [being a public company](/handbook/being-a-public-company/).

## Biggest risks

We have a page which documents our [biggest risks](/handbook/leadership/biggest-risks/). Many of our [values](/handbook/values/) help to mitigate some of these risks.
