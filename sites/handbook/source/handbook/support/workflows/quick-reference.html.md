---
layout: handbook-page-toc
title: Support Quick Reference
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Tools Used by Request Type

The following table contains links to the various tools you may need to use in order to follow the workflows above.


| Request               | [Risk Factor Worksheet]((https://drive.google.com/drive/u/0/search?q=Risk%20factor%20worksheet%20parent:1nI4lCILooN-0U_RmPJP6_cNyIDgXJR99) | [API Username Lookup](https://gitlab.com/api/v4/users?search=emal@email.com) | [GitLab.com Admin](https://gitlab.com/admin/users?utf8=%E2%9C%93&search_query=email@example.com) | [CustomersDot - Admin](https://customers.gitlab.com/admin/) | [Mailgun](https://app.mailgun.com/app/logs/mg.gitlab.com?date_from=2018-11-08T00%3A00%3A00.000Z&date_to=2018-11-14T23%3A59%3A59.999Z&sort=datetime%3Adesc) |
| ------- |:------:|:------:|:------:|:------:|:------:|
| 2FA | X | X | X |   |   |
| Password Reset |   | X | X |   | X |
| Invoice Request |   |   |   | X |   |
| Change Credit Card |   |   |   | X |   |
| Cancel Trial |   |    |   | X |   |
| Cancel + Refund |   |    |   | X |   |
| Dormant Username |   | X | X |   |   |
| Blocked by Security |   |   |   |   |   |
|                     |   |   |   |   |   |

#### Tools Tips
* [API Username Lookup](https://gitlab.com/api/v4/users?search=emal@email.com): Use in Firefox for an easier to read, parsed output
* [CustomersDot - Admin](https://customers.gitlab.com/admin/): Shared login, password in vault.
* [Mailgun](https://app.mailgun.com/app/logs/mg.gitlab.com): Shared login, password in vault. Make sure you switch the log view to `mg.gitlab.com`
