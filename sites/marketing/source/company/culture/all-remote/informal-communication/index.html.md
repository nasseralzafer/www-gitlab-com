---
layout: handbook-page-toc
title: "Informal Communication in an all-remote environment"
canonical_path: "/company/culture/all-remote/informal-communication/"
description: How to foster informal communication in a remote company
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how informal communication occurs at GitLab, how it complements [in-person interactions](/company/culture/all-remote/in-person/), and why it matters in an all-remote [culture](/company/culture/#culture-at-gitlab). There are over 20 different ways to foster informal communication below, and we are constantly discovering and adding new methods.

## Formally design informal communications

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">When working remote it is important to formalize informal communication. Explicitly plan time to create, build, and maintain social connections and trust. In our handbook we list 15 methods <a href="https://t.co/OAYB1uKX3r">https://t.co/OAYB1uKX3r</a> which I&#39;ll summarize in this thread.</p>&mdash; Sid Sijbrandij (@sytses) <a href="https://twitter.com/sytses/status/1250946875143815168?ref_src=twsrc%5Etfw">April 17, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
In colocated environments, informal communication is naturally occurring. When individuals are physically located in the same space, there are ample opportunities to chitchat and carry on conversations outside of formal business settings. 

Making social connections with coworkers is important to building trust within your organization. One must be **intentional** about designing **informal** communication when it cannot happen more organically in an office.

![Group social calls are a great way for remote teams to connect and bond](/images/all-remote/gitlab-marketing-team-hotdog-meeting.jpg){: .shadow.medium.center}
Group social calls are a great way for remote teams to connect and bond
{: .note.text-center}

Informal communication is important, as it enables friendships to form at work related to matters *other* than work. Those who feel they have genuine friends at work are [more likely to enjoy their job](https://onlinelibrary.wiley.com/doi/full/10.1111/peps.12109), perform at a high level, feel invested in the company, and serve others within the organization. At GitLab, we desire those outcomes as well, reinforcing our [Results value](/handbook/values/#results). 

For all-remote companies, leaders should not expect informal communication to happen naturally. There are no hallways for team members to cross paths in, no carpools to the office, etc. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Ajkw2-neqPU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [video](https://youtu.be/Ajkw2-neqPU) above, GitLab co-founder and CEO Sid Sijbrandij discusses informal communication within an all-remote company with Vlad Lokshin, co-Founder and CEO at [Turtle](https://www.turtle.dev/).

> If you do all-remote, do it early, do it completely, and change your work methods to accommodate it. Be intentional about informal communication. All-remote forces you to do the things you should be doing anyway, earlier. - *GitLab co-founder and CEO Sid Sijbrandij*

In an all-remote environment, informal communication should be formally addressed. Leaders should organize informal communication, and to whatever degree possible, design an atmosphere where team members all over the globe feel comfortable reaching out to anyone to converse about topics unrelated to work. 

## Create opportunity for chance meetings

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">To what extend do you think chance meetings can be organized? For some examples see <a href="https://t.co/OAYB1uKX3r">https://t.co/OAYB1uKX3r</a></p>&mdash; Sid Sijbrandij (@sytses) <a href="https://twitter.com/sytses/status/1270721630537277442?ref_src=twsrc%5Etfw">June 10, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

In colocated settings, unplanned encounters can lead to interesting ideas or hypotheses, which can sprout innovation. In remote settings, leadership should create opportunities for these "chance meetings" to still occur.

### Remote retrospectives

Intentionally organized [remote retrospectives](/blog/2019/12/19/how-gitlab-handles-retrospectives/) create an atmosphere of chance discussions, as you're specifically there to riff on something. Zoom supports [Breakout Rooms](https://support.zoom.us/hc/en-us/articles/206476093-Enabling-breakout-rooms), which may be useful in catalyzing such conversation. 

### Innovation communities

Encourage your workforce to join remote communities which exist to create chance meetings. [Open Assembly](https://open-assembly.com/) is one such network, which "hosts the conversations, community and learning that help culture and business transition to the future of work." This creates a highly focused session where everyone arrives ready to engage with new people, find common ground, and share insights — all of which feed back into the culture and energy of one's workplace. 

### Conferences and events

Remote teams have a tendency to savor and anticipate [in-person engagements](/company/culture/all-remote/in-person/). As in-person interactions are less common in a remote role, this presents an opportunity for leadership to maximize the utility of conferences and events. 

Leaders should consider sending teams to conferences as an efficient way to organize a dense series of chance meetings, with each person then returning to saner spaces to process, distill, and put effort into newly seeded ideas.

### The power of local communities

Global remote work creates tremendous opportunity for innovation to emerge from a more diverse array of locales. As remote teams are empowered to fill a part of their social quota by local interactions with neighbors and members of their home communities, they will experience a much richer set of inputs. 

Consider a weekly [asynchronous](/company/culture/all-remote/asynchronous/) debrief where remote team members share lessons learned and challenges faced in their home communities, which may spark spin-off conversations within the workplace linked to new products or solutions. 

## Devote time to fostering relationships

![GitLab marketing team Show & Tell social call](/images/all-remote/marketing-social-call-show-and-tell.jpg){: .shadow.medium.center}
GitLab marketing team Show & Tell social call
{: .note.text-center}

In all-remote environments, there should be a greater emphasis placed on carving out time to get to know one another as humans. To connect and bond as empathetic beings with interests, emotions, fears, and hopes — people, not just colleagues. 

If you've spent any length of time in a corporate setting, you've probably seen a company institute a weekly or monthly "happy hour," designed to gather employees in a shared space to converse about topics unrelated to work. 

For colocated companies, the occasional team offsite — to take in a sporting event, to enjoy a shared lunch, etc. — may be enough to supplement naturally occurring informal communication in the office. 

Below are a number of intentional facets of GitLab's culture, created to foster informal communication. We welcome other all-remote companies to iterate on these and implement as desired. 

- [Social call](/handbook/communication/#social-call): A series of optional calls throughout the week in which everyone in the organization is invited to. We have no set agenda, just a time set aside for everyone to openly talk where everyone is a moderator.
- [Contribute Unconference](/company/culture/contribute/): An in-person, week-long event where we bring the entire company together in one location to get to know each other better.
- [Group conversations](/handbook/people-group/group-conversations/): Four times a week the company gets together virtually to discuss an area of the business. Slides are provided for context but not presented.
- Coffee chats: More details below.
- Coworking calls: More details below. 
- Social hours: Informal social calls organized within our immediate teams to get to know each other on a more personal level. 
- [Visiting grants](/handbook/incentives/#visiting-grant): This travel stipend encourages team members to visit each other by covering transportation costs up to $150 per person they visit.
- Local meetups: Co-located team members are encouraged to organize their own meetups, whether it's a coworking space or getting dinner together. 
- [CEO house](/handbook/ceo/#house): Team members can get together in Utrecht, Netherlands, at the CEO's AirBnB, free of charge. 
- [Slack](/handbook/communication/#slack): We use Slack channels for informal communications throughout the company, whether it's a team-specific channel or a channel dedicated to sharing vacation photos with other team members. 
- [Zoom calls](https://about.gitlab.com/handbook/tools-and-tips/#zoom): Not only do we get to know our coworkers better by seeing them in real time during video calls, we also get to know their pets and families too. This visual engagement helps us relate to each other on a more personal level, so when we meet in person, we already know each other. In fact, when our team members meet face-to-face for the first time, the most surprising factor is usually each person's height.

>  **"I’ve been given a tour of team members’ new houses, admired their Christmas decorations, squealed when their pets and kids make an appearance and watched them preparing dinner – glimpses into the personal lives of my colleagues that I’ve never had in any office job."** - [Rebecca](https://about.gitlab.com/company/team/#rebecca), Managing Editor, GitLab

## Creating room for human connection on calls

![Mixing it up with hats and backgrounds](/images/all-remote/gitlab-marketing-social-hour.jpg){: .shadow.medium.center}
Mixing it up with hats and backgrounds
{: .note.text-center}

In a colocated setting, meetings tend to begin with personal conversation. Virtual calls may feel less amenable to that, so a team has to be intentional about creating space for empathy and human connection. A rolling calendar of calls that are purely about work can lead to [burnout](/company/culture/all-remote/mental-health/). At GitLab, we accomplish this through the following methods.

### Speedy meetings

As part of our overall [Communications Handbook](/handbook/communication/#scheduling-meetings), we prefer to utilize the "speedy meetings" setting in Google Calendar, which encourages before-the-call banter and conversation. It also provides space between meetings to recharge and reset. 

### Lead with empathy

Each work-related call should begin with an earnest, genuine "How are you?," or a similar and appropriate introduction. It's important to remember that everyone is facing a battle that you know nothing about, and in a remote setting you should actively listen. 

### Welcome interruptions in calls

We should welcome pets, children, deliveries, neighbors, or partners interrupting a call. This is an opportunity to bond and to humanize the work experience. Take a few minutes to talk to the person if they are open to it, or ask the team member to share more about their pets/family.

## Using emojis to convey emotion

Though emojis have commonly been reserved for personal conversations that occur outside of the workplace, all-remote employees should feel comfortable using them in everyday discourse with team members.

[Perception has shifted on using emojis in professional settings](https://www.wsj.com/articles/yes-you-actually-should-be-using-emojis-at-work-11563595262). In Slack alone, north of 26 million custom emojis have been created since the feature was introduced. In all-remote settings, where you may never meet a colleague in person, leveraging visual tools to convey nuance in tone, emphasis, and emotion can lead to more empathy and a tighter human connection.

> Using emoticons, emoji, and stickers can supplement the lack of human nonverbal cues in computer-mediated environment. The [results](https://link.springer.com/chapter/10.1007/978-981-10-8896-4_16?mod=article_inline) show that proper use of emoticons, emoji, and stickers, especially positive emoticons, is conducive to both relationship formation and cognitive understanding. They not only help participants express emotions and manage interrelations but also function as words to aid message comprehension. - *Ying Tang and Khe Foon Hew, researchers at the University of Hong Kong*

Too, emojis can create a more [inclusive](/company/culture/inclusion/) communication environment. When you're working with colleagues where the de facto business language isn't someone's first language, more universal indicators (e.g. "eyes" for "I've seen this" or "smile" for positivity) can reduce the mental burden of deciphering a message. 

## Talent shows

![GitLab marketing talent show](/images/all-remote/gitlab-marketing-talent-show.jpg){: .shadow.medium.center}
GitLab marketing talent show
{: .note.text-center}

Talent shows are great for building meaningful connections between colleagues, and are relatively easy to organize. Simply add a calendar invite, affix a Google Doc agenda to the invite for team members to sign up to showcase a talent, arrange a panel of 3-5 judges, and have a list of prizes ready to go. 

If you organize these a week or two in advance, it enables team members to prepare. GitLab's Marketing team [hosted a talent show on Zoom](https://twitter.com/darrenmurph/status/1243194687453835265), with over 130 people on a video call. The moderator called on colleagues based on how they were arranged in the Google Doc, and the judges tallied up results at the end. 

## Virtual tour

During a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) or a [show and tell](/company/culture/all-remote/informal-communication/#show-and-tell), take a moment to provide a virtual tour of where you live and work. This allows you to virtually "invite someone over" while enabling others to get to know you better. You can do this informally (while walking around with your laptop) or you can choose to make it a friendly competition within your team using recorded videos (à la [Architectural Design's Open Door](https://youtu.be/xlNC8Q62imU) style). 

This was done during the [Threat Management](/handbook/engineering/development/threat-management/) sub-department's team day. They included a "Virtual Home Tour" as one of the activities and they [recorded videos](https://drive.google.com/file/d/1go2xx8IoPUW7SiLSKU3vqWDBOYKx5dT8/view) to share with the team (*videos only available internally to GitLab*).

## Coffee chats

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CH-NHoBOeho?start=496" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the Traction Conference [video](https://youtu.be/CH-NHoBOeho) above, GitLab co-founder and CEO Sid Sijbrandij shares how remote teams can stay connected with Frederic Lardinois of [TechCrunch](https://techcrunch.com/).

We understand that working remotely leads to mostly work-related conversations with fellow team members, so everyone at GitLab is encouraged to dedicate **a few hours a week** to having social calls with anyone in the company. 

It's a great chance to get to know who you work with, talk about everyday things and share a coffee, tea, or your favorite beverage. We want you to make friends and build relationships with the people you work with to create a more comfortable, well-rounded environment. 

#### Scheduling a Coffee Chat

GitLab Team Members can easily schedule a Coffee Chat in Google Calendar and link it to Zoom, with a 1-click link to the video call.

Watch this [short video](https://youtu.be/zbbEn_PznK0) to see how it's done.

1. Search for the person in Google Calendar
2. Click the time and name the meeting **Name / Name coffee-chat**
3. Click on *Make it a Zoom meeting*

Google Calendar will warn you if the time is outside the other person's working hours, and you can select from a list of suggested times.

#### Diversifying your Coffee Chats

Coffee Chats are a fantastic opportunity to intentionally broaden your perspective at GitLab. Some suggestions on intentional Coffee Chats:

1. Pair with someone in a group/stage that have **some** overlap with yours, but with whom you don't interact regularly.
1. Pair with someone in a group/stage that your team has dependencies with, but that you don't know well (for example, Customer Success + Customer Support).
1. Pair with someone from a different region of the world (being respectful of working hours).
1. Pair with someone complementary to you — for example, if you're new, find someone who's been here a while; if you're in marketing, pair with a developer; and so on.

You can use the [GitLab org chart](/company/team/org-chart/), [Product stages page](/handbook/product/product-categories/), or [Team page](/company/team/) to find team members to pair with.

##### The Donut Bot

Team members can join the #donut_be_strangers Slack channel to be paired with a random team member for a coffee chat. The "Donut" bot will automatically send a message to two people in the channel every other Monday. 
Please schedule a chat together, and Donut will follow up for feedback.
Of course, you can also directly reach out to your fellow GitLab team-members to schedule a coffee chat in the #donut_be_strangers Slack channel or via direct message.

#### GitLab Team Member Mixer Calls
Similar to a coffee chat, a "[Gitlab Team Member](/handbook/communication/top-misused-terms) Mixer" call aims to help team members meet more folks from other groups or functions that they might not otherwise get to spend a lot of time working or interfacing with.  

To set up a GitLab Team Member Mixer, two team-members set up a single Zoom line.  Using the single Zoom line, each of those team members invites someone else to join - making a total of four team members on the call.  This way, you'll get to meet someone new - or find connections between team members you didn't know existed.  Each "host" brings a topic to discuss in case the conversation needs help getting started.

At the end - the host and their co-host's _guest_ set up another GitLab Team Member Mixer call - with two new guests.  In this way, the participation gets spread throughout the organization and connects as many team members as possible.

#### Juice Box Chats
Similar to a coffee chat, a "Juice Box Chat" is a chance for GitLab team members and their children, grandchildren, or other members of their family to get to know one another as well.  These chats can be informal or can focus on a specific topic: Legos, Super Heroes, camping, and video games all make for great subjects.

As we work in an all-remote environment, kids and other household members often show up to calls unannounced, which is just part of working remotely.  However, Juice Box Chats allow us to meet and socialize this way intentionally.  Think a "bring your kid to work day" except in an all-remote company.

You can take a look at the [internal time zones and interests document](https://docs.google.com/spreadsheets/d/1SSeFAMLKWuBZ9Ku32SzorKV_KHW5jTqTVAOovr3HeGM/edit#gid=0) to find a group that might work for you or join our `#kids-juicebox-chats` channel in Slack.

## Team Social Calls

![Spreading aloha on a GitLab company call](/images/all-remote/GitLab-Aloha-Shirt-Team.jpg){: .shadow.medium.center}
Spreading aloha on a GitLab company call
{: .note.text-center}

Some teams at GitLab organize informal social calls on a regular basis in order to build camaraderie. 
The [data team](/handbook/business-ops/data-team/#-contact-us) has them every Tuesday. 
Team members and managers are encouraged to create these calls as a medium for informal, agenda-free interaction between team members. 

## Show and Tell

Consider creating a shared calendar for social events to more broadly allow people to join and be aware of social events that are in a more convenient time zone. For example, GitLab's [marketing](/handbook/marketing/) team has a "Show and Tell" call where team members are encouraged to display something they've crafted and share the story behind it.

Arranging this is simple. Send a calendar invite to a team or department with a shared document attached. Those interested in showing can sign up in a numerical list with their names, so that it's simple to hop from one person to the next on the eventual video call. 

Show and Tell sessions are excellent ways to humanize the work experience, and to learn about colleagues on a deeper, more personal level. It showcases what they're proud of, and what provides fulfillment outside of work.

## Team DJ Zoom Room

![Time to get the virtual DJ room started in Zoom](/images/all-remote/zoom-share-computer-music-dj-room.jpg){: .shadow.medium.center}
Time to get the virtual DJ room started in Zoom
{: .note.text-center}

[Zoom](/handbook/tools-and-tips/#zoom) added a unique feature within its `Share Screen` function, enabling a remote team to share audio from their computer with all who are in the Zoom meeting. This is useful for getting an entire team together in a shared virtual space without everyone feeling compelled to speak and socialize. A shared DJ Room is ideal for scenarios where team members need to be mostly heads-down on work, but appreciate the closeness of the team and the shared musical experience.

To create additional interaction, hand off the proverbial DJ duties every 2-3 songs, enabling an array of team members to share their favorite tunes. This can trigger fun conversation on preferred music genres and artists

To utilize this feature in Zoom, see below.

* Click the upward arrow beside the `Share Screen` icon
* Click the `Advanced` option tab at the top of the screen
* Click `Music or Computer Sound Only`
* Play music from any app or website, being mindful that you'll want to lower the volume so that your voice can also be heard

More instructions on this can be found at [this support page](https://support.movegb.com/how-to-play-music-over-your-live-stream-on-zoom). 

## Create a company songbook

If the Team DJ Zoom Room strikes a proverbial chord with your team, consider creating a company songbook. Get inspired by visiting the [GitLab Songbook](/company/culture/songbook/).

## AMA (Ask Me Anything)

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

Hosting an [AMA](/company/culture/all-remote/learning-and-development/#ask-me-anything-ama-group-conversations-and-key-meetings) is a great way for people to learn about others. While questions *can* be about work, these serve as an opportunity to ask funny and far-fetched questions that enable a more personal connection. It's particularly important to consider ongoing AMAs with executives. AMAs humanize leaders and remind teams that we are [all more alike than we are unalike](https://allpoetry.com/Human-Family).  

## Celebrations and holidays

![A bit of Airplane! fun on Halloween](/images/all-remote/GitLab-Halloween-2019-Costume-Zoom-Call.jpg){: .shadow.medium.center}
A bit of Airplane! fun on Halloween
{: .note.text-center}

A team that is distributed across the globe creates opportunity for many celebrations. Different countries and cultures celebrate in their own way, enabling team members to gain an understanding of key dates and events that matter to colleagues. A culture that encourages a team to thoughtfully express their celebrations on company calls is a healthy, inclusive one. 

### Global pizza parties/meals

Teams can also arrange shared meals around the world. [Global pizza parties](/blog/2019/10/02/support-virtual-pizza-party/), for example, are possible to document and enjoy in a shared setting (Zoom or Slack), though one may wish to consider a breakfast pizza depending on time zone. 

## Virtual lunch table

![Cheers to a fantastic team](/images/all-remote/gitlab-zoom-happy-hour.jpg){: .shadow.medium.center}
Cheers to a fantastic team
{: .note.text-center}

An easy way to create an opportunity for team members to connect over a virtual meal is to maintain a videoconference room that serves as a rolling lunch table. 

Adventurous team members could even to cook on camera, and the host may consider utilizing Zoom's `Share Music` [feature](/company/culture/all-remote/informal-communication/#team-dj-zoom-room) to pipe music into the virtual gathering spot. 

Sharing a meal is a powerful way to connect as humans, particularly when you open it up globally. This creates a more casual atmosphere where team members can connect with colleagues on a more personal level, without work at the center of the video call. 

## Virtual scavenger hunt

![Virtual scavenger hunt](/images/blogimages/cs-scavenger-hunt/hugo.jpg){: .shadow.medium.center}
Virtual scavenger hunt at GitLab
{: .note.text-center}

GitLab's Customer Success team ideated and executed a [worldwide scavenger hunt](https://gitlab.com/gitlab-com/customer-success/tam/issues/212) using GitLab (the product), which is [detailed on the GitLab Unfiltered blog](/blog/2020/04/06/cs-scavenger-hunt/) and is easily replicated. 

## Virtual trivia

GitLab's global [marketing team](/handbook/marketing/) plays pub-style trivia every other week. You can compete as individuals or as teams, with a suggesting time of 1 hour. For those within the GitLab organization, email `events@gitlab.com` if you want to arrange a game for your team. 

GitLab utilizes [MysteryTrip](http://games.mysterytrip.co/), which serves large teams well and doesn't place a heavy preparation load on the organizer. MysteryTrip handles the preparation work for questions, acts as trivia master, and has automatic scoring and a leadercard. This makes spinning up a game quick and easy, with pricing set at around $20 per player. For those outside of the GitLab organization, regularly scheduled trivia sessions are an excellent way to take everyone's focus away from work and engage in a shared experience.  

## Coworking calls

These video calls are scheduled working sessions on Zoom where team members can work through challenging tasks with a coworker, or simply hang out while each person works on their own tasks. 
This recreates a productive working session you might have in person in a traditional office setting, but from the comfort of your own desk. 
Want to try advanced mode? Screen share as you work together (keeping in mind any confidentiality issues).

## Humanizing the digital experience

In the wake of COVID-19, many suddenly-remote companies are seeking to humanize the digital experience. For companies which were not intentional about structuring informal communication in an office, this void becomes glaring in a remote environment. 

In this section, we'll spotlight tips and advice from other experts and companies.

[Bretton Putter](https://twitter.com/BrettonPutter), founder at [CultureGene](https://twitter.com/culturegenehq), shares the below [examples](https://twitter.com/BrettonPutter/status/1263834004341563393/photo/1). 

1. Book, TV, and movie clubs
1. Movie nights leveraging [Netflix Party](https://www.netflixparty.com/)
1. Group game/dance/music nights via a streaming service like [Twitch](https://www.twitch.tv/)
1. Lunch and Learn webinars (ideally learning about something not directly work related, such as playing an instrument)
1. Group fitness, yoga, and meditation sessions
1. Conference call bingo
1. Regular team coffee break rooms
1. Collaborative quizzes 
1. Green/yellow/red personal status check-ins
1. [Geekbot](https://geekbot.com/) "What I Did This Weekend" polls for teams on Slack
1. Dress-up/hat themes for team or all-hands video calls

### Tools for building empathy and connection

1. Build empathy, rapport, and cohesion on remote teams with [Kona by Sike Insights](https://sikeinsights.com/)
1. Asynchronous voice messaging for teams via [Yac](https://www.yac.com/)
1. Asynchronous video messaging for teams via [Loom](https://www.loom.com/)

## GitLab Knowledge Assessment: Informal Communication in an All-Remote Environment 

Anyone can test their knowledge on Informal Communication in an All-Remote Environment by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSceCZyY1gYk-fpLbPDpdQcRRxZtTT5wz5xyULrlKZBadvHusg/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been completed, you will receive an email acknowledging the completion from GitLab with a summary of your answers. If you complete all knowledge assessments in the [Remote Work Foundation](/company/culture/all-remote/remote-certification/), you will receive an unaccredited [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

----

Return to the main [all-remote page](/company/culture/all-remote/).
