---
layout: markdown_page
title: "GitLab vs GitHub for DevSecOps"
description: "Learn more about Gitlab DevSecOps capabilities missing in Github."
canonical_path: "/devops-tools/github/GitLab-vs-GitHub-for-DevSecOps.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab DevSecOps Capabilities Missing in GitHub

| GitLab Capability                                                         | Features                                                                |
|---------------------------------------------------------------------------|-------------------------------------------------------------------------|
| View all security issues in a single pane of glass within project context | Security Dashboard                                                      |
| Proactively scan for vulnerabilities                                      | Dependency scanning, Container Scanning                                 |
| Security test static code with a single click                             | Static Application Security Testing                                     |
| Preview App before Merge to reduce defects, shorten development time      | Preview changes with review apps. Environments Autostop for review apps |
| Security Test running applications                                        | Dynamic Application Security Testing                                    |
