---
layout: markdown_page
title: "CircleCI"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab-CircleCI Comparison Infographic
  This summary infographic compares CircleCI and GitLab across several DevOps Stages and Categories.

![GitLab CircleCI Comparison Chart](/devops-tools/circleci-vs-gitlab/GitLab_CircleCI.jpg)

## Summary

Founded in 2011 and headquartered in San Francisco, Ca., CircleCI  provides a service that automates the Continuous Integration stage of the Software Development Life Cycle (SDLC).  Their CI service offering can be hosted in the cloud or on a private server and can only integrate with two SCM vendors: GitHub Enterprise and Bitbucket (CircleCI Cloud).  CI jobs are built within four different environments: a Docker image, a Linux VM, Windows VM, or a MacOS VM.  They demonstrate their support of the Open Source Community by providing organizations with free credits for Open Source builds.

**CircleCI Orbs**

CircleCI can provide automated services for other stages of the Software Development Life Cycle (SDLC) using third party plug-ins that they call “Orbs”.  They define Orbs as reusable/sharable packages of YAML configurations that condenses repeated pieces of configs into a single line of code. In other words, think of Orbs as a pointer that is included in the YAML configuration file that activates a piece of code during the build process that performs a function.  The Orbs are housed in an open source code library.

## CircleCI to GitLab Decision Kit

* [CircleCI for the Business Decision Maker](https://about.gitlab.com/devops-tools/circleci/CircleCI-BDM.html)

* [GitLab Over CircleCI Key Differentiators](https://about.gitlab.com/devops-tools/circleci/GitLab-Over-CircleCI-Key-Differentiators.html)

* [CircleCI License](https://about.gitlab.com/devops-tools/circleci/CircleCI-License.html)

* [How to Migrate from CircleCI to GitLab](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/32487)

## Comparison
