/* eslint-env jasmine */
/* eslint-disable global-require */
/* global Promise */
const { mount } = require('@vue/test-utils');
const component = require('../../source/frontend/components/salary-calculator/SalaryCalculator.vue').default;

describe('the salary calculator', function () {
  var SalaryCalculator;

  var mockData;

  var getMockData = () => ({
    equityCalculator: {
      equity_grades: [{
        grade: 5,
        options: 2000
      },{
        grade: 8,
        options: 15000
      }, {
        grade: 10,
        options: 20000
      }]
    },
    contractTypes: [{
      country: 'United States',
      employee_factor: 1,
      entity: 'GitLab Inc'
    }, {
      country: '*',
      contractor_factor: 1.17,
      entity: 'GitLab BV'
    }, {
      country: 'Canada',
      employee_factor: 1,
      entity: 'GitLab Inc'
    }],
    countryNoHire: [
      'Brazil'
    ],
    currencyExchangeRates: [{
        currency_code: 'CAD',
        rate_to_usd: 0.76088,
        rate_from_usd: 1.3018,
        symbol: 'C$',
        countries: ['Canada']
      }
    ],
    locationFactors: [{
      country: 'United States',
      area: 'Columbus, Ohio',
      locationFactor: 47.3
    }, {
      country: 'Canada',
      area: 'Alberta',
      locationFactor: 55.00
    }, {
      country: 'Canada',
      area: 'Calgary',
      locationFactor: 55.00
    }],
    roles: [{
      title: 'Backend Engineer',
      levels: 'engineering_ic',
      ic_ttc: {
        compensation: 160000,
        percentage_variable: 0,
        from_base: true
      },
      director_ttc: {
        compensation: 275000,
        percentage_variable: 0.15,
        from_base: true
      }
    }, {
      title: 'Engineering Management, Quality',
      levels: 'eng_manager_srmgr_director',
      manager_ttc: {
        compensation: 252000,
        percentage_variable: 0,
        from_base: true
      },
      director_ttc: {
        compensation: 275000,
        percentage_variable: 0.15,
        from_base: false
      }
    }, {
      title: 'No level',
      levels: false,
      ic_ttc: {
        compensation: 10000,
        percentage_variable: 0,
        from_base: true
      }
    }],
    roleLevels: {
      engineering_ic: [{
        title: 'Junior',
        factor: 0.8,
        type: 'ic_ttc'
      }, {
        title: 'Intermediate',
        factor: 1.0,
        is_default: true,
        type: 'ic_ttc'
      },{
        title: "Staff/Engineering Manager",
        factor: 1.0,
        grade: 8,
        type: 'manager_ttc'
      }, {
        title: "Director",
        factor: 1.0,
        grade: 10,
        type: 'director_ttc'
      }],
      eng_manager_srmgr_director: [{
        title: "Staff/Engineering Manager",
        factor: 1.0,
        grade: 8,
        is_default: true,
        type: 'manager_ttc'
      }, {
        title: "Senior Engineering Manager",
        factor: 1.2,
        grade: 8,
        type: 'manager_ttc'
      }, {
        title: "Director",
        factor: 1,
        grade: 10,
        type: 'director_ttc'
      }]
    }
  });

  jest.mock('jquery', () => {
    return {
      get: () => jest.fn()
    }
  });

  beforeEach(function () {
    SalaryCalculator = mount(component);
    mockData = getMockData();
    SalaryCalculator.vm.setData(mockData);
  });

  describe('methods', function () {
    describe('setRoleLevels', function () {
      it('sets the default level if none are available', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[2] });
        SalaryCalculator.vm.setRoleLevels();

        expect(SalaryCalculator.vm.currentLevel).toEqual({ title: 'N/A', factor: 1, type: 'ic_ttc' });
      });

      it('sets the current level from the role, if available', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setRoleLevels();

        expect(SalaryCalculator.vm.currentLevel).toEqual(mockData.roleLevels.engineering_ic[1]);
      });
    });

    describe('setParamsFromUrl', function () {
      it('sets values from the URL', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?country=United+States&area=Columbus,+Ohio&level=Junior');

        expect(SalaryCalculator.vm.currentCountry).toEqual('United States');
        expect(SalaryCalculator.vm.currentArea.area).toEqual('Columbus, Ohio');
        expect(SalaryCalculator.vm.currentLevel.title).toEqual('Junior');
      });

      it('skips params not in the URL', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0], currentCountry: null });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Intermediate');

        expect(SalaryCalculator.vm.currentCountry).toBe(null);
        expect(SalaryCalculator.vm.currentLevel.title).toEqual('Intermediate');
      });

      it('only sets areas valid for the country', function () {
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?country=Canada&area=Columbus,+Ohio');

        expect(SalaryCalculator.vm.currentCountry).toEqual('Canada');
        expect(SalaryCalculator.vm.currentArea).toBe(null);
      });
    });

    describe('formatAmount', function () {
      it('formats the amount in USD by default', function () {
        expect(SalaryCalculator.vm.formatAmount(1234)).toEqual('$1,234');
      });

      it('formats the amount in another currency code when passed', function () {
        expect(SalaryCalculator.vm.formatAmount(1234, 'AUD')).toEqual('1,234 AUD');
      });
    });

    describe('formatAreaLocationFactor', function () {
      it('formats the number to three decimal places', function () {
        expect(SalaryCalculator.vm.formatAreaLocationFactor({ locationFactor: 0.12345 })).toEqual('0.123');
      });

      it('returns "--" when null is passed', function () {
        expect(SalaryCalculator.vm.formatAreaLocationFactor()).toEqual('--');
      });
    });

    describe('findByCountry', function () {
      it('finds a result matching the country name', function () {
        expect(SalaryCalculator.vm.findByCountry(mockData.contractTypes, 'United States')).toEqual(mockData.contractTypes[0]);
      });

      it('falls back to * when there is no match', function () {
        expect(SalaryCalculator.vm.findByCountry(mockData.contractTypes, 'Somewhere else')).toEqual(mockData.contractTypes[1]);
      });
    });

    describe('currentLevelTTCType', function () {
      it('sets the current level ttc type from the role, if available', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setRoleLevels();

        expect(SalaryCalculator.vm.currentLevelTTCType).toEqual('ic_ttc');
      });

      it('changes the ttc type when the level is different', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director');

        expect(SalaryCalculator.vm.currentLevelTTCType).toEqual('director_ttc');
      });
    });

    describe('currentTTCValues', function () {
      it('reads the right values, depending on the role level', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setRoleLevels();

        expect(SalaryCalculator.vm.currentTTCValues).toEqual(mockData.roles[0]['ic_ttc']);
      });

      it('updates the values when changing the role level', function() {
        SalaryCalculator.setData({ currentRole: mockData.roles[0]});
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director');

        expect(SalaryCalculator.vm.currentTTCValues).toEqual(mockData.roles[0]['director_ttc']);
      });
    });

    describe('calculateBasePay', function () {
      it('equals the full salary when there is no percentage', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Intermediate&country=Canada&area=Alberta');

        expect(SalaryCalculator.vm.calculateBasePay(0.85)).toEqual(SalaryCalculator.vm.calculateSalary(0.85));
      });

      it('calculates the base pay correctly when there is a percentage variable', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director&country=Canada&area=Alberta');
        expect(SalaryCalculator.vm.calculateBasePay(0.85)).toEqual(111794);
      });
    });

    describe('calculateVariablePay', function () {
      it('returns 0 when the role has no variable percentage', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Intermediate&country=Canada&area=Alberta');

        expect(SalaryCalculator.vm.calculateVariablePay(0.85)).toEqual(0);
      });

      it('equals the basepay * the percentage when there is a percentage', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director&country=Canada&area=Alberta');
        expect(SalaryCalculator.vm.calculateVariablePay(0.85)).toEqual(Math.round(SalaryCalculator.vm.calculateBasePay(0.85) * 0.15));
      });

      it('equals the fullsalary * percentage when it is not calculated from base', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[1] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director&country=Canada&area=Alberta');
        expect(SalaryCalculator.vm.calculateVariablePay(0.85)).toEqual(Math.round(SalaryCalculator.vm.calculateSalary(0.85) * 0.15));
      });
    });

    describe('calculateSalary', function () {
      it('basePay === calculateSalary when the role has no variable percentage', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Intermediate&country=Canada&area=Alberta');

        expect(SalaryCalculator.vm.calculateSalary(0.85)).toEqual(74800);
      });

      it('calculates the pay correctly when there is a percentage variable', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director&country=Canada&area=Alberta');
        expect(SalaryCalculator.vm.calculateSalary(0.85)).toEqual(128563);
      });
    });

    describe('localCurrencyVariablePayRange', function () {
      it('returns null when there is no variable percentage', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Intermediate&country=Canada&area=Alberta');

        expect(SalaryCalculator.vm.localCurrencyVariablePayRange).toEqual(null);
      });

      it('calculates the range correctly when there is a percentage variable', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Director&country=Canada&area=Alberta');
        expect(SalaryCalculator.vm.localCurrencyVariablePayRange).toEqual("20,742 CAD - 31,114 CAD");
      });
    });
  });
});
