Process Improvements? Have suggestions for improving the release post process as we go?
Capture them in the [Retrospective issue](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/TODO).

- **Preview page** (shows latest merged content blocks for reference till the 17th): https://about.gitlab.com/releases/gitlab-com/
- **View App** (shows introduction, MVP and latest merged content blocks for reference 18th - 21st: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html
- **click to tweet URL** (Use this link to tweet, will be available on the 20th or before): <TBD>

_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/sites/marketing/source/releases/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/X_Y
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Features YAML Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/images/feature_page/screenshots
- **Homepage card**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/sites/marketing/source/includes/home/ten-oh-announcement.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/community/release-managers/

| Release post manager | Tech writer | Messaging | Social |
| --- | --- | --- | --- |
| `@release_post_manager` | `@tw_lead` |`@pmm_lead` | DRI: `@wspillane` & `@social` for Slack Checklist item |

### Release post kickoff (`@release_post_manager`)

**Due date: YYYY-MM-DD** (By the 7th)

**Before starting on this checklist, you should have created the release post branch and required files [as explained in the Handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-your-release-post-branch-and-required-directoriesfiles)**

- [ ] Verify this MR is labeled ~"blog post" ~release ~"release post" ~"priority::1" and assigned to you (the Release Post Manager)
- [ ] Add the current milestone to this MR
- [ ] Create a release post retrospective issue ([example](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7918)) and update the link at the top of this MR description
- [ ] Replace each `@mention` in this MR description with the names of the Release Post Manager, Tech Writer, Messaging Lead, and Social Lead for this release
- [ ] Update the links in this MR description
- [ ] Update all due dates in this MR description
- [ ] Make sure the release post branch has all initial files: `sites/marketing/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, `data/release_posts/X_Y/mvp.yml` and `data/release_posts/X_Y/cta.yml`
- [ ] Add the release number and your name as the author to `sites/marketing/source/releases/posts/YYYY-MM-22-gitlab-X-Y-release.html.md`
- [ ] Per guidance on [communication](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communication) for the release, create X-Y-release-post-prep channel in Slack and invite `@tw_lead` and `@pmm_lead`
- [ ] Set up a 15 minute weekly standup with the TW lead and Messaging lead to touch base and troubleshoot. If times zones conflict, this is not mandatory.
- [ ] In the #release-post Slack channel, remind Product Managers that all [content blocks (features, recurring, bugs, etc.)](#content-blocks) should be drafted, and under review by the 10th. All direction items and notable community contributions should be included in the release post.


### Recurring items starting on the 12th: `@release_post_manager`

**General Content Review**
As PMs finalize their release post items it can be helpful for the RPM to review and offer feedback. This reduces backpressure on the 17th as items are merged and provides additional review from someone with a fresh perspective. You can start this as early as the 12th, but this should be an ongoing task leading up to content assembly on the 18th. Review each MR labeled ~Ready for general content structure and writing quality, along with ensuring PMs are answering "why" the feature is being released, not just "what".

To easily manage and track reviewed items do the following:

- [ ] Bookmark a filtered MR list similar to [this](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20post%20item&not[label_name][]=rp%20manager%20reviewed&label_name[]=release%20post&label_name[]=Ready) to track release post items you haven't reviewed. (note: add the correct milestone to that filter)
- [ ] Add the ~"rp manager reviewed" label to any RP item you've reviewed.


**Reminding and Alerting DRIs**
It's important to keep DRIs up to date regularly with items they need to deliver for the release post. Especially given how async and distributed GitLab team members are early reminders are very helpful.

- [ ] Alert DRIs (PMs, EMs and others as needed) at least one working day before each due date (post a comment to #release-post Slack channel)

### General contributions `@release_post_manager`

The release post is authored following a changelog style system.
Each item should be in an individual YAML file.

#### Contribution instructions

See [Handbook: Contributing to the release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#general-contributions).

#### Content blocks

**Due date: YYYY-MM-DD** (10th)

Product Managers are responsible for [raising MRs for their content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#pm-contributors) and ensuring they are reviewed by necessary contributors by the due date. Content blocks should also be added for any epics or notable community contributions that were delivered.

Product Managers are also responsible for making sure all [required (Tech Writing) and recommended (PM Director and PMM) reviews ](https://about.gitlab.com/handbook/marketing/blog/release-posts/#reviews) get done for their content blocks. To help reviewers prioritize what to review, PMs should communicate which content blocks are most important for review by applying the proper labels to the release post item MR prior to assigning the MR to reviewers. (ex: Tech Writing, Direction, Deliverable, etc). PMs can also [follow these guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#recommendations-for-optional-director-and-pmm-reviews) to help decide which content blocks should get PM Director and PMM reviews.

To enable Engineering Managers [to merge the content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merging-content-block-mrs) as soon as an issue has closed, PMs should ensure all scheduled items have MRs created for them and have the Ready label applied when content contribution and reviews are completed.

Product Managers should only check their box below when **all** their content blocks (features, deprecations, etc.) are complete (documentation links, images, etc.). Please don't check the box if there are still things missing.

_Reminder: be sure to reference your Direction items and Release features._ All items which appear
in our [Upcoming Releases page](https://about.gitlab.com/upcoming-releases/) should be included in the relevant release post.
For more guidance about what to include in the release post please reference the [Product Handbook](https://about.gitlab.com/handbook/product/product-processes/#communication#release-posts).

- Manage
  - [ ] Access (`@mushakov`)
  - [ ] Analytics (`@jshackelford`)
  - [ ] Compliance (`@mattgonzales`)
  - [ ] Import (`@hdelalic`)
- Create
  - [ ] Source Code (`@danielgruesso`)
  - [ ] Knowledge (`@cdybenko`)
  - [ ] Static Site Editor (`@ericschurter`)
  - [ ] Editor (`@phikai`)
  - [ ] Gitaly (`@derekferguson`)
  - [ ] Gitter (`@ericschurter`)
  - [ ] Ecosystem (`@deuley`)
- Plan
  - [ ] Project Management (`@gweaver`)
  - [ ] Portfolio Management (`@kokeefe`)
  - [ ] Certify (`@mjwood`)
- Verify
  - [ ] Continuous Integration (`@thaoyeager`)
  - [ ] Runner (`@DarrenEastman`)
  - [ ] Testing (`@jheimbuck_gl`)
  - [ ] Pipeline Authoring (`@dhershkovitch`)
- Package
  - [ ] Package (`@trizzi`)
- Release
  - [ ] Progressive Delivery (`@ogolowinski`)
  - [ ] Release Management (`@jreporter`)
- Configure
  - [ ] Configure (`@nagyv-gitlab`)
- Monitor
  - [ ] Health + APM (`@sarahwaldner`)
- Secure
  - [ ] Static Analysis (`@tmccaslin`)
  - [ ] Dynamic Analysis (`@derekferguson`)
  - [ ] Composition Analysis (`@NicoleSchwartz`)
  - [ ] Fuzz Testing (`@stkerr`)
- Defend
  - [ ] Container Security (`@sam.white`)
  - [ ] Threat Insights (`@matt_wilson`)
- Growth
  - [ ] Fulfillment (`@mkarampalas`)
  - [ ] Retention (`@mkarampalas`)
  - [ ] Product Analytics (`@jeromezng`)
  - [ ] Expansion (`@timhey`)
  - [ ] Conversion (`@s_awezec`)
  - [ ] Acquisition (`@jstava`)
- Enablement
  - [ ] Distribution (`@ljlane`)
  - [ ] Geo (`@fzimmer`)
  - [ ] Memory (`@joshlambert`)
  - [ ] Global Search (`@JohnMcGuire`)
  - [ ] Database (`@joshlambert`)

#### Recurring content blocks

**Due date: YYYY-MM-DD** (10th)

The following sections are always present and managed by the PM or Eng lead
owning the related area.

- [ ] Add GitLab Runner improvements: `@DarrenEastman`
- [ ] Add Omnibus improvements: `@ljlane`
- [ ] Add Mattermost update to the Omnibus improvements section: `@ljlane`

**Due date: YYYY-MM-DD** (10th)

- [ ] [Add Performance Improvements](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-bugs-and-performance-improvements) section: `@release post manager`
- [ ] [Add Bug Fixes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-bugs-and-performance-improvements) section: `@release post manager`

#### Final Merge

**Due date: YYYY-MM-DD** (17th)

Engineering managers listed in the MRs are responsible for merging as soon as the implementing issue(s) are officially part of the release. All release post items must be merged on or before the 17th of the month. Earlier merges are preferred whenever possible. If a feature is not ready and won't be included in the release, the EM should push the release post item to the next milestone.

To assist managers in determining whether a release contains a feature. The following procedure [documented here](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34519) is encouraged to be followed. In the coming releases, Product Management and Development will prioritize automating this process both so it's less error prone and to make the notes more accurate to release cut.

### Content assembly and initial review (`@release_post_manager`)

Note: `Final Content Assembly`, `Structural Check` and `Messaging Lead Review` steps all happen in sequence on the 18th starting ~8am PST (`America/Los_Angeles`). If the Release Post Manager, TW Lead, and Messaging Lead span many timezones it's recommended you coordinate ahead of the 18th to understand how this could impact working hours for each team member. If need be, the time of initiated the `final content assembly` and the subsequent coordinated tasks can be shifted, as long as `Final content review` with the CEO and EVP Product begin no later ~12pm PST, to allow enough time for feedback/updates.  


**Due date: YYYY-MM-DD** (12th or earlier)

- [ ] In the comments in this MR, mention `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers` and ask them to add their team Performance Improvements and Bugs Fixes to the MRs you've created, referencing links to the specific MRs
- [ ] In Slack #release-post, share the link to the Bug Fixes MR and ask PMs to work with their EM to add high impact bugs to the MR
- [ ] In the #release-post Slack channel, [request MVP nominations](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp)

**Due date: YYYY-MM-DD** (15th or earlier)

- [ ] Select a [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image).
- [ ] Verify that the selected cover image has not been used before.
  - Tip: MacOS users, navigate to the `source/images/` directory and use the search bar in the Finder to search for `cover`. Make sure the scope is set to only search "images". This won't reveal all previous images, but the last couple years have had pretty consistent naming.
- [ ] On the `release-X-Y` branch, add the cover image to `source/images/X_Y/X_Y-cover-image.jpg`. Tip: Be sure to use an `_` between release numbers, not a `-`
- [ ] In `sites/marketing/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, [add details about the source image](https://about.gitlab.com/handbook/marketing/blog/release-posts/#cover-image-license).
- [ ] Choose an [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) for this release
  - [ ] If no MVP nominations have been posted to #release-post by the 15th, send a reminder to #release-post and solicit additional recommendations by posting to #core and #community-relations.
  - [ ] If one or more quality nominations have been received, choose one and notify the Product team in #release-post of your choice. Use this chance to solicit any last-minute nominations and confirm that the contribution your pick was nominated for will make it into this release.
- [ ] Remind `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers` to add their team Performance Improvements and Bug Fixes to the MRs you've created, referencing links to the specific MRs

**Due date: YYYY-MM-DD** (17th or earlier)

- [ ] Mention the [Distribution PM](https://about.gitlab.com/handbook/product/product-categories/#distribution-group) reminding them to add any relevant [upgrade warning](https://about.gitlab.com/handbook/marketing/blog/release-posts/#important-notes-on-upgrading) by doing an [upgrade MR](https://about.gitlab.com/handbook/marketing/blog/release-posts/#upgrades)
- [ ] Ask the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) to finalize the introduction by the 18th
- [ ] Check with the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) to see if they need help pinning down the final themes/features for the intro
- [ ] If there are no deprecation MRs, ask in the #release-post Slack channel if there are any deprecations to be included
- [ ] Finalize your [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) selection and work with the nominator of the MVP to write the MVP section in `data/release_posts/X_Y/mvp.yml` on the `release-X-Y` branch
- [ ] Make sure the PM for the MVP feature adds a corresponding content block if applicable, linking from the MVP section
- [ ] On the `release-X-Y` branch, add the MVP's name and other profile info to `data/mvps.yml`

**Due date: YYYY-MM-DD** (17th or earlier)

- [ ] Confirm your local dev environment is running a current version of Ruby. 
      The `.ruby-version` in this repository file will have the recommended Ruby version.
      Enter `ruby -v` on the command line if you don't know or need to compare.
      Some Ruby version managers may alert you to the fact that you do not have the correct version installed. 

- [ ] If you have an outdated Ruby version installed and **no version manager** follow these steps to install latest version of Ruby:
  - [ ] Install [Homebrew](https://brew.sh) (if you haven't already). Visit the link for the installation command.
  - [ ] Install some dependencies required for [asdf](https://asdf-vm.com/#/core-manage-asdf?id=dependencies) (the recommended version manager) `brew install coreutils curl git`
  - [ ] Install asdf with the following command `brew install asdf`
  - [ ] Modern versions of macos use ZSH for the shell. You'll need to run the following command to include asdf into the shell configuration `echo -e "\n. $(brew --prefix asdf)/asdf.sh" >> ~/.zshrc`
  - [ ] Older versinos of macos use Bash for the shell. You would instead run the following: `echo -e "\n. $(brew --prefix asdf)/asdf.sh" >> ~/.bash_profile`
  - [ ] ASDF uses a plugin for each language it can manage. Install the Ruby plugin with the follwoing: `asdf plugin-add ruby https://github.com/asdf-vm/asdf-ruby.git`
  - [ ] The asdf-ruby plugin can detect the Ruby version required from the `.ruby-version` file, but that needs to be configured. Run the following: `echo "legacy_version_file = yes" > ~/.asdfrc`
  - [ ] Finally, install the current Ruby version `asdf install ruby 2.6.5` (current at the time of this writing)
  - [ ] Optionally, you can set this version of Ruby globall and not just to this directory with `asdf global ruby 2.6.5`
- [ ] If you have a different Ruby Version Manager like [rbenv](https://github.com/rbenv/rbenv) already installed, then you likely just need to update Homebrew with `brew upgrade rbenv ruby-build` and install the latest with `rbenv install 2.6.5` similar to the last step above. 

- [ ] How do I know if I have a Ruby Version Manager installed? If you type `which asdf` or `which rbenv` and the output is sommething other than `asdf not found` or `rbenv not found` then you likely have one of these installed. 

**Due date: YYYY-MM-DD** (18th at 8 AM PT and NO earlier)

- [ ] Perform **final content assembly** by pulling all content block MRs merged to master into the release post branch by using the following commands locally (one command at a time):
  ```
  git checkout master
  git pull
  git checkout release-X-Y
  git pull
  bin/release-post-assemble
  git push
  ```
- [ ] Do a visual check of the release-X-Y content block and image folders to make sure paths and names are correct
- [ ] Make sure the release post branch View App generates as expected
- [ ] Notify the PM team in #release-post Slack channel, including a link to the View App, that final content assembly has happened and all work must now shift from master onto the release post branch via coordination with the release post manager. In the same Slack post, remind PMs  they need to check the View app to make sure all their content is showing up as expected with correct image/video links, etc.  

**Note**: If the release post assembly script fails, look at the bottom of [this section](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merge-individual-items-in-to-your-branch) of the release post handbook page for further instruction

**Due date: YYYY-MM-DD** (18th)
- [ ] Remind PMs to review their content blocks for accuracy in the View app and check off their [content and recurring blocks](#content-blocks) by tagging them into this MR
- [ ] Label this MR: ~"blog post" ~release ~"review-in-progress"
- [ ] Check if there are no broken links in the View App (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
  - [ ] Links to confidential issues may be missed. It is helpful to check for broken links as an unauthenticated GitLab user (either logged out, in another browser, or in Incognito mode).
- [ ] Check all comments in the MR thread (make sure no contribution was left behind)
- [ ] Make sure all discussions in the thread are resolved
- [ ] Add a sentence in the introductory paragraph to state how many features are shipped in addition to the top features (see [13.3 release post](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/marketing/source/releases/posts/2020-08-22-gitlab-13-3-released.html.md#L26) as an example)
- [ ] Assign the MR to the next reviewer (structural check)

### Other reviews

Ideally, complete the reviews by the 19th of the month, so that the 2 days before the release can be left for fixes and small improvements.

#### [Marketing content and positioning](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) (Messaging Lead)

**Due date: YYYY-MM-DD** (13th - 20th)

The Marketing content, positioning and reviews are done by the Messaging lead: `@pmm_lead`

- [ ] Starting any time after the 22nd of the previous month and until final merge on the 17th of the next month, you can look at [X-Y Preview page](https://about.gitlab.com/releases/gitlab-com/) for the latest features available on .com that will likely get packaged for the current release cycle on the 22nd
- [ ] Before the 13th, you should have a shortlist of top release themes/features - process listed in [release post handbook page](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead). One handy way to look for the most impactful features is to filter release post items labeled with ~"release post item::primary"
- [ ] On the 13th, post a message on #release-post slack channel (no need to directly ping anyone) for feedback from the product team on top features (give a list of 4-5 themes or 8-10 features)
- [ ] Before the 14th, commit the first (rough) draft of the release post introduction with the various themes/features selected above in YYYY-MM-DD-gitlab-X-Y-released.html.md in the release X-Y branch. Notify the TW lead that the draft introduction is available
- [ ] Before the 15th and after you've had a chance to get feedback from the rest of the product team on the 13th, share your initial themes/features as a note/thread in Slack (not the MR) with the EVP of Product `@sfwgitlab` and the VP of Product `@adawar` in #release-post for early feedback. This way they have a chance to provide input sooner rather than later
- [ ] After EOD on the 17th, you can look for merged release post items with label 'Ready' which indicate features making it in the release, to update your draft introduction based on any changes in the release. At this time you should also be able to reference the latest View app the release post manager publishes on the 18th.
- [ ] Before the 18th, check/copyedit feature blocks (optional as it is reviewed by PMM leads for the stages)
- [ ] Before the 18th, check/copyedit `features.yml` (optional as it is reviewed by PMM leads for the stages)
- [ ] By the 18th, [reorder primary features](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-order) by stage and/or according to their relevance per the theme of your release post intro (to re-order adjust the `primary` and `top` filenames to an alphanumeric order e.g. `01.yml` `02.yml`). Reordering primary features is an option but not required.
- [ ] By the 18th, post your top 3 themes/features in #release-post slack channel. Make sure to cc the Head of Products (`@adawar` and `@sfwgitlab` and Directors
- [ ] By the 18th, after receiving feedback from Heads of Product and Directors, revise as required and let the Release Post Manager know when updates are complete so they can begin [Final content review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44776#final-content-review)
- [ ] After the release post manager successfully runs content block assembly on the morning of 18th, use the View App to identify and add the correct anchor links for the features included in release post introduction. Ping the TW lead once this is complete and assign them to the issue.

The following steps typically happen after [Structural Check](#structural-check-technical-writing-lead) is complete:

- [ ] Before the 20th, add a video link to the kickoff call recording at the bottom of the release post introduction. You can find the video once it's available on the [direction page](https://about.gitlab.com/direction/kickoff/)
- [ ] Before the 20th, update `data/release_posts/X_Y/cta.yml` with any relevant CTA (Commit promotion, for example). Ensure that `X_Y` in the file path reflects the current release. If there isn't a specific call to action to add, use this as the default button text: "Join us for an upcoming event" with a link to '/events/'. Reference [this example from 13.1](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52057/diffs#diff-content-fc2520a82e843973f9c87fded973c2555a4f3bbf) under `data/release_posts/13_1/cta.yml` if needed.
- [ ] Before the 20th, on the `release-x-y` branch, update the release blurb for the homepage (check /sites/marketing/source/includes/home/ten-oh-announcement.html.haml). In the announcement file, update rows 3,8 and 11 with the relevant details for the current release including both release number and month.
  - [ ] Before 20th, ensure that the social sharing text for the tweet announcement is available in the introduction
  - [ ] By the 20th, tag @social in the release post MR, link to the blog hero chosen image, and provide the blog title. A member of the social team will then create the image needed, by adding two commits the release post MR. For more details look at [the social sharing image](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image)
  - [ ] On 20th, assign the MR back to the Release Post Manager
- [ ] **After the 22nd of the release month, review this list and update as required**

#### [Structural check](https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check) (Technical Writing Lead)

**Due date: YYYY-MM-DD** (18th)

The structural check is performed by the technical writing lead: `@tw_lead`

For suggestions that you are confident don't need to be reviewed, change them locally
and push a commit directly to save the PMs from unneeded reviews. For example:

- clear typos, like `this is a typpo`
- minor front matter issues, like single quotes instead of double quotes, or vice versa
- extra whitespace

For any changes to the content itself, make suggestions directly on the release post
diff, and be sure to ping the reporter for that block in the suggestion comment, so
that they can find it easily.

- [ ] Add the label ~review-structure.
- [ ] Check [frontmatter](https://about.gitlab.com/handbook/marketing/blog/release-posts/#frontmatter) entries and syntax.
- [ ] Check that images match the context in which they are used, and are clear.
- [ ] Check all images (png, jpg, and gifs) are smaller than 150 KB each.
- [ ] Check for duplicate entries.
- [ ] Check that features introduced in this release do not reference the two previous releases (this often happens as features may slip to a future release after an RPI is already written)
- [ ] Check all dates mentioned in entries, ensuring they refer to the correct year.
- [ ] Remove any `.gitkeep` files accidentally included.
- [ ] Add or check `cover_img:` license block (at the end of the post). Should include `image_url:`, `license:`, `license_url:`.
- [ ] Check the anchor links in the intro after the message lead has notified the TW lead that this is complete. All links in the release post markdown file should point to something in the release post Yaml file.
- [ ] Run a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf) and ping reporters directly on Slack asking them to fix broken links.
  - [ ] Links to confidential issues may be missed. It is helpful to check for broken links as an unauthenticated GitLab user (either logged out, in another browser, or in Incognito mode).
- [ ] Run a spelling check against the Release Post's View app. For example, using [Webpage Spell-Check](https://chrome.google.com/webstore/detail/webpage-spell-check/mgdhaoimpabdhmacaclbbjddhngchjik?hl=en) for Google Chrome.
- [ ] Ensure content is balanced between the columns (both columns are even).
- [ ] Report any problems from structural check in the `#release-post` channel by pinging the reporters directly for each problem. Do NOT ping `@all` or `@channel` nor leave a general message to which no one will pay attention. If possible, ensure open discussions in the merge request track any issues.
- [ ] Post a comment in the `#whats-happening-at-gitlab` channel linking to the View App + merge request reminding the team to take a look at the release post and to report problems in `#release-post`. CC the release post manager and messaging lead. Template to use (replace links):
  ```md
  Hey all! This month's release post is almost ready! Take a look at it and report any problems in #release-post.
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/1234
  View app: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html
  ```
- [ ] Remove the label ~review-structure.
- [ ] Assign the MR to the next reviewer (Marketing content and reviews).
- [ ] Within 1 week, update the release post templates and release post handbook with anything that comes up during the process.

#### [Social image creation](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image) (Social Team) `@social`
   - [ ] On the 20th, on the `release-X-Y` branch, add [the social sharing image](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image) to `source/images/tweets/gitlab-X-Y-released.png`.
   - [ ] On the 20th, on the `release-X-Y` branch, in the Intro, update the '/images/tweets/gitlab-X-Y-released.png' to reference the social sharing image filename

#### Final content review (`@release_post_manager`)

**Due date: YYYY-MM-DD** (18th - 19th)

- [ ] Check to be sure there are no broken links in the View app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Mention `@sytse`, `@sfwgitlab`, and `@adawar` in #release-post on Slack when the post has been generated for their review
- [ ] Capture any feedback from Slack into a single comment on the Release Post MR with action items assigned to the DRIs to address. More info [here](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-reviews)

**Incorporating Feedback - Due date: YYYY-MM-DD** (by the 20th)

- [ ] Make sure all feedback from CEO and Product team reviews have been addressed by working with DRIs of those areas as needed
- [ ] If you receive feedback about the ordering Primary Items work with the Messaging Lead to determine how you might adjust the order.
- [ ] If applicable re-order Secondary items by adjusting the `titles` in the content blocks. More information to consider about altering secondary items [here](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-reviews) | [technical instructions](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-order)
- [ ] Make sure there are no open feedback items in this MR or in Slack #release-post channel
- [ ] On 20th, ping VP Product Management (`@adawar`) for final check in Slack #release-post
- [ ] After VP Product Management review, remove the label ~review-in-progress

**Prepare Social Links - Due date YYYY-MM_DD** (by the 20th)

- [ ] Check the "click to tweet" URL for sharing the merged Release Post. DRI: `@wspillane` & `@social`
- [ ] Update "click to tweet" URL in the description of this Merge Request

### Preparing to merge to master (`@release_post_manager`)
#### On the 21st

- [ ] Make sure the [homepage card](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/marketing/source/includes/home/ten-oh-announcement.html.haml) was updated by the Messaging lead on the release X-Y branch
- [ ] Check with the Messaging lead to confirm the social sharing copy is finalized (should be visible in View app at this point)
- [ ] Lock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) on the `master` branch with File Locking **on the 21st**
- [ ] Mention `@advocates` on Slack #swag to ask them to send the swag pack to the MVP
- [ ] Check if all the anchor links in the intro are working
- [ ] Confirm there are no broken links in the View app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Do a visual check of the blog post and ordering of content blocks in their relevance to the intro
- [ ] Update the final features count in the introductory paragraph

#### On the 22nd (`@release_post_manager`)

#### At 12:30 UTC

- [ ] Read the [important notes](#important-notes) below
- [ ] Release Managers will alert you in `#release-post` if there are any issues with the release. You can follow along on the release issue to see the packaging progress on the 22nd | [issue list](https://gitlab.com/gitlab-org/release/tasks/-/issues/) [example issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/1261). The `#releases` slack channel is also a good place to track any updates or announcements.
  - If anything is wrong with the release, or if it's delayed, you must ping
  the messaging lead on `#release-post` so that they coordinate anything scheduled
  on their side (e.g., press releases, other posts, etc).
  - If everything is okay, the packages should be published at [13:30 UTC](https://gitlab.com/gitlab-org/release-tools/-/blob/fac347e5fc4e1f31cffb018d90061ef4f25747f3/templates/monthly.md.erb#L104-125), and available publicly around 14:10 UTC.
- [ ] Check to make sure there aren't any alerts on Slack `#release-post` and `#whats-happening-at-gitlab` channels
- [ ] Check to make sure there aren't any alerts on this MR

### Merging to master (`@release_post_manager`)
#### At 13:50 UTC

Once the release manager confirmed that the packages are publicly available by pinging you in Slack:

- [ ] Unlock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) just before merging.
- [ ] Merge the MR at 14:10-14:20 UTC.
- [ ] Wait for the pipeline. This can take anywhere from 20-45 minutes to complete.
- [ ] Check the live URL on social media with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/).
- [ ] Check for broken links again once the post is live.
- [ ] Handoff social posts to the social team and confirm that it's ready to publish: Mention @social in the `#release-post` Slack channel; be sure to include the live URL link and social media copy (you can copy/paste the final copy from the View app).
     - A member of the social team will schedule the posts at the next available best time on the same day. The social team will mark the Slack message with a ⏳ once scheduled and add scheduled times to the post thread for team awareness. Further details are listed below in the Important Notes Section.
- [ ] Share the links to the post on the `#release-posts` and
`#whats-happening-at-gitlab` channels on Slack.

#### What to do if your pipeline fails

For assistance related to failed pipelines or eleventh-hour issues merging the release post, follow the [Handbook on-call](https://about.gitlab.com/handbook/about/on-call/) process by asking for assistance in the `#handbook-escalation` Slack channel. Cross post the thread from #handbook-escalation in #release-post so all Product Managers and release post stakeholders are aware of status and delays.

#### Important notes

- The post is to be live on the **22nd** at **15:00 UTC**. It should
be merged and as soon as GitLab.com is up and running on the new
release version (or the latest RC that has the same features as the release),
and once all packages are publicly available. Not before. Ideally,
merge it around 14:20 UTC as the pipeline takes about 40 min to run.
- The usual release time is **15:00 UTC** but it varies according to
the deployment. If something comes up and delays the release, the
release post will be delayed with the release.
- Coordinate the timing with the [release managers (https://about.gitlab.com/community/release-managers/). Ask them to
keep you in the loop. Ideally, the packages should be published around
13:30-13:40, so they will be publicly available around 14:10 and you'll
be able to merge the post at 14:20ish.
- Once the release post is live, wait a few minutes to see if no one spots an error (usually posted in #whats-happening-at-gitlab or #company-fyi), then follow the `handoff to social team` checklist item above.
- The tweet to share on Slack will not be live, it will be scheduled during a peak awareness time on the 22nd. Once the tweet is live, the social team will share the tweet link in the `#release-post` and in the `#whats-happening-at-gitlab` Slack channels.
- Keep an eye on Slack and in the blog post comments for a few hours
to make sure no one found anything that needs fixing.



/label ~"blog post" ~release ~"release post" ~"priority::1"
/assign me
